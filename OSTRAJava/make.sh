#!/bin/bash

#načti cesty
in_dir=$(pwd)
out_dir=$(pwd)/../out

#pokud out dir existuje tak ho vyčisti
if [[ -d $out_dir ]]
then
    rm -R $out_dir
fi

#vytvoř znova out dir
mkdir $out_dir

#přesuň se do adresáře OstraJavy
cd $(dirname "$(readlink -f "$0")")

#zkompiluj
./ostrajavac $in_dir -d $out_dir
#spust
./ostrajava -h 2048 -f 512 -s 256 $out_dir "$@"

#přesuň zpět
cd $in_dir
