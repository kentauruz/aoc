banik pyco

tryda QuickSort {
    statyk zerad(cyslo[] pole, cyslo spodek, cyslo vrsek) {
        kaj (spodek < vrsek) {
            toz cyslo pi = QuickSort.rozdel(pole, spodek, vrsek) pyco

            QuickSort.zerad(pole, spodek, pi - 1) pyco
            QuickSort.zerad(pole, pi + 1, vrsek) pyco
        }
    }

    statyk cyslo rozdel(cyslo[] pole, cyslo spodek, cyslo vrsek) {
        toz cyslo p = pole[spodek] pyco
        toz cyslo i = spodek pyco
        toz cyslo j = vrsek pyco

        rubat(i < j) {
            rubat(pole[i] <= p aj i <= vrsek - 1) {
                i = i + 1 pyco
            }

            rubat(pole[j] > p aj j >= spodek + 1) {
                j = j - 1 pyco
            }
            kaj (i < j) {
                QuickSort.vymen(pole, i, j) pyco
            }

        }
        QuickSort.vymen(pole, spodek, j) pyco
        davaj j pyco
    }

    statyk vymen(cyslo[] pole, cyslo a, cyslo b) {
        toz cyslo t = pole[a] pyco
        pole[a] = pole[b] pyco
        pole[b] = t pyco
    }
}

fajront pyco
