﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2023_6(bool firstPuzzle)
		{
			var time = new int[] { 46, 82, 84, 79 };
			var distance = new int[] { 347, 1522, 1406, 1471 };

			var time2 = new int[] { 7, 15, 30 };
			var distance2 = new int[] { 9, 40, 200 };

			//(t - x) * x > d => tx - x^2 - d > 0 => -1x^2 + tx + -d > 0
			//D = t^2 - 4 * -1 * -d = t^2 - 4d
			//x1 = (-t - sqrt(D)) / 2 * -1
			//x2 = (-t + sqrt(D)) / 2 * -1

			var margin = 1L;

			if (firstPuzzle)
			{
				for (var i = 0; i < time.Length; i++)
				{
					var t = time[i];
					var d = distance[i];

					margin *= Compute(t, d);
				}
			}
			else
				margin = Compute(46828479, 347152214061471);

			Console.WriteLine($"Margin of error {margin}");
		}

		static long Compute(long time, long distance)
		{
			var D = time * time - 4 * distance;
			if (D > 0)
			{
				var sqrtD = Math.Sqrt(D);
				var x1 = (-time - sqrtD) / -2;
				var x2 = (-time + sqrtD) / -2;

				var t1 = (long)Math.Ceiling(Math.Min(x1, x2));
				var t2 = (long)Math.Floor(Math.Max(x1, x2));

				if (t1 == Math.Min(x1, x2))
					t1++;
				if (t2 == Math.Max(x1, x2))
					t2--;

				return t2 - t1 + 1;
			}
			else
				throw new Exception(string.Format("Not solvable {0}. D = {1}", time, D));
		}
	}
}
