﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_17(bool firstPuzzle)
		{
			var target = (X: (Min: 139, Max: 187), Y: (Min: -89, Max: -148));
			//var target = (X: (Min: 20, Max: 30), Y: (Min: -5, Max: -10));

			var minXVel = (int)(Math.Sqrt(2 * target.X.Min));
			if (minXVel * (minXVel + 1) / 2 < target.X.Min)
				minXVel++;
			var maxXVel = (int)(Math.Sqrt(2 * target.X.Max));
			if (maxXVel * (maxXVel + 1) / 2 > target.X.Max)
				maxXVel--;

			var maxH = 0;
			var maxY = 0;
			for(int xVelInit = minXVel; xVelInit <= maxXVel; xVelInit++)
			{
				for(int yVelInit = 0; yVelInit < Math.Abs(target.Y.Max); yVelInit++)
				{
					var x = 0;
					var h = 0;
					var yVel = yVelInit;
					var xVel = xVelInit;
					var hit = false;
					var hMax = 0;
					do
					{
						x += xVel;
						if (xVel > 0)
							xVel--;
						h += yVel--;

						if (h > hMax)
							hMax = h;
						if (x >= target.X.Min && x <= target.X.Max && h <= target.Y.Min && h >= target.Y.Max)
						{
							hit = true;
							break;
						}
					}
					while (x <= target.X.Max && h >= target.Y.Max);

					if (hit)
					{
						if (hMax > maxH)
						{
							maxH = hMax;
							maxY = yVelInit;
						}
					}
				}
			}

			if (firstPuzzle)
				Console.WriteLine("Max height: {0}", maxH);
			else
			{
				var hits = 0;

				for (int xVelInit = minXVel; xVelInit <= target.X.Max; xVelInit++)
				{
					for (int yVelInit = target.Y.Max; yVelInit <= maxY; yVelInit++)
					{
						var x = 0;
						var h = 0;
						var yVel = yVelInit;
						var xVel = xVelInit;
						do
						{
							x += xVel;
							if (xVel > 0)
								xVel--;
							h += yVel--;

							if (x >= target.X.Min && x <= target.X.Max && h <= target.Y.Min && h >= target.Y.Max)
							{
								hits++;
								break;
							}
						}
						while (x <= target.X.Max && h >= target.Y.Max);
					}
				}

				Console.WriteLine("Total hits: {0}", hits);
			}
		}
	}
}
