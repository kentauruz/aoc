﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_20(bool firstPuzzle)
		{
			var iea = @"######.#..##..######.....#...#.#...#.######.#.#...#..#..#..###.#####.#.####...#.#.#...#.#.#...#####..###.....#.#.....#.#.#..###..#.#.#####..#.....####.##.##..#..##.#.##.##..##.#####.####.#.#....#...#...#...#.#########..#..#####..#.#.###....#.##.###...##.#..#....##.###...#..##.#..#...#.#.#####.####...#.##..##..#.#######...#..##.##.....#..#..#.###...###.######..##.#..##.######....#.##.##...###.##..#.#.#.#########....####.####.#.#...#.#.#..#.#.##.#...#.#..#######..###..##.#..####.###...#.#.#.##.#####.##.###.#.";
			var input = @".##.##...#####.#######.##.##.#####..#.#.#.##.....##...#..#..###.#....###...##.....###......#..##...#
#..#...#.##..#.#..#....#...#...#....####..#.....#.#...##.#..##.##.##...#.#..#..####...#....##.##.##.
...#.#..#..###...##..#.##########...##...#.#.#.#.#####..###..###..##.#..#####..###......##.#.##...##
#..###.##.##...#.#..#.....#.##.#..#...#..####.#.#.##.###....#.#..#..##.#....#.#.#.###...###.#.####.#
...###.....###...##...#..######.###...##.#.##.###.#.#....###.##.###.###.###....######..##.#.#...#..#
###...#.##.#.###..#..#.####.#...#..#.#.#..#.#..#....###..##..####.##.##....#..##.#.##..#.#..#.#.##..
.###.##.#.....#.#.##.##.####.#..#.#..###.##...###..##..##.#.#...#..#.#####...#.####.#....######.#..#
..##.........##.##......##..#.#.#....#..####..#.#..###.##..#.##..####.#.#.##.#.#.##.....##....#.####
#..###..#...###.##.#.#....#.#....###....###...##.#..####.#####.#.##.#...#.#.#.##..#....#..#.#...##.#
###.#..#...#.##.#.#.#..#.####.###...##.####.#.#.........#.##..####.###..#.#...###..#.##..##...####..
#....###.##.##...###...##.###...#..##....#.##.#.##.#.##..#..##..#..#..#...#.##..##.###.#..##.#..###.
.#.#......##..#..#....#.##.#..##.##....#....#..###..##.##..###..#..#.#.##.#.##..#...#.#....#.#...#.#
#.#..#....#.##.##..#.###..##..####.##...##.#.#.###.#.#..#...#.##..#..#.###.....#.##.#####..#......#.
##.##...#.....##.....###.#..##...#.#...#.##..#........#######...#.###.#.#.##..##.#...###..#...#...#.
....###...###.#.#.#.###..#..####..######....###....####..##..#..#.#.###.#.#.#####...#..#.#.####...##
....#######.#....##...#####...###...#...#...#.#...#.###.#.####.#...#.#.#.###....#..#....#.#.#..##..#
#.####.###.#..#.#.#..#####.......##....####.##.####...#..###..#......#..#.#...#....##.....#.........
#..###.#..##.##..###.#.#.#.###.#....#.####.#.###.##.##..#...#.#.#..#..###..#..##......#.####..#.#.##
###.#..##.###....##.#####.###.....#....#####.#..#.#.#.##..#...##...###.##.##...###.##..#..#...#.#..#
#.#...#..#..#.###.#..#..##.#.#.....#.#.#....#.##...##.......####..#...#..#.......###..#.#.#....#..##
.##...##..#..#..####.##.......#...#.###..#..##.#.#.#.##..#..##.###.##.#.#.#..##.##.##..#..###.#..#.#
#...#.###.#......#...#.......##..##...##...#.#....##.#.##.##....##.######..##.##.####.#####....#.#..
#.##.####....##....#..#.#..#.####.#.####.###..##..#.....##.###.#...#...#..#..##.##.###...###.###.#.#
....#.##.##.###.#...#....#...###.#.#......#..#.###.#.#.#.#...#.#..###..#.##.#...##.######..#..####..
####...#.#.###.#...#.......##..#.#######...##...##..##......###.#####.#.#.#.###.#..#.#..##.##..###.#
#.#......#.#.####..##.##...#..######..#.#..#...#..#.####.#.##.#####.#..##..##.#.###.#.#.##.#.#.##.#.
..#.#..#.#...###.#..#.#.##.#.#....###.#..###..##..#######.#.##.####.....#.####.#.##.#...#####......#
#######...##.#.#.###..###.#..##..##...####..###.#.##.##.##...#.##.##.##....#.#..#.###...###.#.#.#...
.##.##..####..#.###########....#...#.#..##......##......#.#..###.##....#..#..#.#..###########..##.##
#....##...###..#.#.###.##......###.#.###.######.#..#.###.#.#.##..#....###.###.#.....#..#..#.#####..#
.######..#.###...###.#..##..#.####.###...###.#.#.####.########.#...#.#.##....####.###.#.##.###...#.#
###..##.##.....#...#.##...#.###...#.#.####..#.##..##..#....#..##.#######.##...#####...##.#...##.##..
###.#.#..###..##.#...####.#..###....###..###..###.###..#.##...#.....#...###.##.#.###.#####.#.##.#.#.
###..######.....#..#.....##.##.##.##...#.#..####....###.#.##...####.#....##.......##.#..###......#.#
##..#.###.##.######.#.##.#...#...###......##.###....##...#.#####.#.##.....#.#..#...####...###...##..
...###..###..##.##..####...##....##.#...#.##..###.###.####...##.#.#.##.#....#.#.###.#..#..#..#.#.##.
..#.###.#.#...#.###..##..##....#.#...#.##.####....#.#.#.#..###.#.#....###.##.#.#...#...#...#..##.#..
###...#.##..##.#..###.#.###.#..#.#.##.#.###...###.#...##.#.......#.#.###....####.....###.##.##.#.#..
.#.####..##.#.#..#######..#..###.#########.#...##..#..##.#.....#.#.##.#.....#.#######..###.....##..#
####.##....#..##.....#...#.#....##.#...####..###.###..#......###.###....##.#.#....###.###.#####..#..
.#.#...##.##.#...#.#...##..###.##..####.###..#....#######..#..##...#####..##...#.#.#######.####.#...
.#.###.######...##.##.#.##...####.#.......#.####.##.#.##.##.####.#...#.#..##....#####.##.#.##..#.#..
..#.#..###.....##....#.###....####.#.##.######..###.##..############.#...#...###.##.#..##.#..####..#
.#.....#.###.#.##....#...#.#####.#.#.##..#...###..##....#..#......#####.#.###..#..####...#####......
.##..#..........##.#.######....##.#.#...###.........#.##..##.......#..#.##......###.....##....#....#
#...#..#.#.#.#.#.####.#..#...#....##.#.#.#....##..#####......##.###.##..#..##...#.##...#....#.##.#..
...#..#.##.####.###.####....##...#......###.###.##....#...##.##.#..#.##.#.....#.##.#.##.##.#.###...#
..#..##.##.#.#####...#..###.#..###..#.####...#.#.##.#...#...#.#.#.###.#.#..##.##..#.##..#....#..###.
..#.#.###...##.#...#.#.#####...###.##...#..###.....#...##.#....##...###.##.####..#..##....#####.##.#
..#.#...#.###...##.##....##.####.....##.##....###..#...#.###..##.#.#.#.##..##...##....#.###.####.#.#
#...#####.##.#.#.#.#...#..###..#.##..##....#..#.###...#.####.###.##.#.##.##......####.....#####..###
.#.#.....#.######.#..#.##..###..#.###.###.##..#.##.....#......#..##.#...###.###..#.#..#.###...#.####
####.##.##.###..#.##.##.#.###.#...##...#..#.###.....#..##..###..#.#..#...#.##.#..#.##.#..#..#.#..#.#
#####.##.####.#######.#.##..##..#.#####..#..#.###..###..#..#.##...#..##.#####...###.#..##.#.###.#..#
...##..##.#.#.###..#.##.#.###....#...###.#####.#.#.#.#......#.###..#..##..#........#.#..#.##..#....#
#.#..#####.##.###...#...###..#.###.......#.#..#..#.#...###....####.###........#.###.####..####.#..#.
........#.##....#.##...#..##...##.####.##.#.##..##......###.#..#.#####..##.#.......##....##..##..#.#
..####.##...###..#####...#..####..###.##.#.#.#.#.....##.##...#.#....###..#..#.##....#...#.####...##.
..##..#.##....##.####.#.#..####.....#.###..#...##..##.#......##...###.....##.....#....##...##.##..##
.#.##...#####..##.##.#..#...#####....#.....#.##.#.##..#...###########...###..#.##.###....#.##.#####.
####......####.#####..###...#.####..#.#..###..#...##..##..#.#.#####.###.##.##..#####.##.#.###.##..#.
##.#..#.####.##..#..#.###.....###.#####.#.#####......#...##..#....##.#.##..###.#####..#.##.#..#..###
.##..####..###....#...#..#.###.#.##..#..#.###..###.##.##...#...##.#..#...#.###..#.###.#.##..##.##.##
#.##..##.#....###.###...#.#.###..#.######..##.#....###.#.###.####..#...#.#..#.#.##.###...##...#.#...
#.#.##.#..#.##....###..###...#...#.#..##.#..##.#..#.##..#.#..####.#....###.#..##..#...#.###.###..#.#
##.##..#####..##.##.##.##..#.....###..#..###..#.##.##.##.#..##.#..#..###.####..#.#####.##.#####.#.##
...###.#...#...###......###.#....##..##.##..########..###....###.####.#..###.######.##.......####.##
##.#.#..#.##.##..#...#...#....#....#....##.#..##.#......#.#.#.#...#.#.####.#.##..#..#.##.##.####...#
##.##...#.#.#.##..#.#....##...###.#####..###..#.###.###.#..###......##.###....##.#.##.####....#####.
.#.#.#####.#......#.##.#.###.#...#.##..#.#....###.##...#...#..##.####..####..###...#..##......###...
###.....##...#######..#.####.....#...#.#...#..#.###.##..#.#....#.#####..##.#.#.#####..#.#.####.#.##.
..#..##..#.#..#...#.#.....##.#.#..##..#.#...#.##.....#...###..##..#.#.#..####..#..#.#.###...#.##.#.#
#...##..#.######.##.#.###.#....#..#.#....#...########.....#####.####....#.##.##..#.#.#...#..##..###.
.#####..####.#..#..#.##.#...#..##.#..##...#....##.#.###......#.......#.....###.....##.####...##..###
##.#...#......###.#.....#..#######.##..#.#.#####..#....##..#..##.#...#.##..#...#.#...#.#.##.#.#####.
#.###..#.####..##..#######....#######..##...###..#.#..#....#.####....#...####.#..###.####...##.#.##.
.#..##..#.#.#.#######...#.#.#.#..#.....###.##..###...#.#.##.#..#.##...#..##..#...#.#...###...####...
...#.#.#.....#.######..##..#.###.###.#.#.###..#....##...###......#.##..#####...###...#...##.#...##..
#.#.......#.##......#...######.....#.###..#####..#.###.##..##...###.##..######...##.#..###.##.#..#.#
....#..##.#.....###.###.#....###..####....#.....###..#.....##.##.##.#..#.#.##...#.##..#.#....###...#
####....#.###..########.##......###....#####.#.#..###.#.#.##.######.#...##...#.###.##.#.....####..##
#####.####....#....###...#.#.#.###.#####..##...#...#.##..#######...#......#...#..####.##......#####.
..##.##.#..#.#.##..##.##..#..#....#.....#######...#..#......#.#.##...#.#.#....###...##..##.#...#.##.
.##.##.#....##.###....#.####....####..##....######.....####.####.#..#.....#####.##.#.##.######....#.
..#...#......##...#..#.#...#.#####..###..#.#..####....#...##.....#.#.#.##.##..#.#.###.##..#.####.#..
.#..#.##.#...#....####.##..##..#####..#..#.#####...##.#.#..####.#...##....#..#####.##.##..##.#..#..#
.##..#.###...#.#..###.###.#......######..#.#.####...#...#..##.########.##..#.###......#.##.#...#.#.#
...#.#..##..#....#..#####.#..######.#..#.#.#...##.######..##..####......#.##.##.#..#..#.##.##...#.#.
#..#.###.####.#.....#...#.....#..##.......#...#..###.###.#.....#.##.#..#.###....##..#...##.#....#...
.##...#..##..##.##...##.##..#.#.#..#.#.#####.####....#....#.#..#..#.#..#...#..#....#.#.#.######....#
#..##.##..#...#....#####....#####...#..#.##.##..##.#..#####.#..####.##.#.#####.#..##.#......##.#..##
#...##.#...#..###....####.##.#.#..#.#.#.#.#..#.#..#..##.#.#.#####...##.##.##.##.##.###...##..#...#.#
#.#..###.....#..####.#.###.######...#######.####..#..##..#.....##.###..#.#.##.###.#.#..##.#.###..###
....###..###.###....####....##..##...#..#...##.#...###..#..#.##...#......###.####...#...#..#.....#.#
#.#..##.##.....####.#..##...#.##.#..#.#.....##.####.#.##.##..#.....####.###.##..##...##...##.#..##.#
######..#.###.##..#......#.###.#.#.....##.###...##..##.###....##....##.##.#.#.#...#..#..#..###..#..#
..#...#...######.##.#.##..#.#.#.#.#.#.#####.####.######.####.##.#.#.#.#....#..#....####.#.##..#.#.##
####..#.##.#..###...##...###.###.####...#.##.####.###.###.##..####.##.#....#..###..#..##.##.#.....#.
.##.##.##.....#######.#..##.#...#.#####.#....##..#...#..##...##...#..#.#..#...#.#..#.###.###.##.###.
...##.#.#.##..#..#..#.#.....##..#.##..##.#..###..#...###......#.###..##..#####..##.###.##....#.##.#.";

			var map = new Dictionary<(int X, int Y), bool>();
			var lines = input.Split('\n');
			var w = lines[0].Length;
			var h = lines.Length;

			var y = 0;
			foreach(var l in lines)
			{
				var x = 0;
				foreach(var ch in l)
				{
					if (ch == '#')
						map.Add((x, y), true);
					x++;
				}
				y++;
			}

			var outsidePixel = 0;
			var offset = 1;
			var area = new (int X, int Y)[]
			{
				(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)
			};

			var repeats = firstPuzzle ? 2 : 50;
			for(int r = 0; r < repeats; r++)
			{
				var newMap = new Dictionary<(int X, int Y), bool>();
				for(y = -offset; y < h + offset; y++)
				{
					for(int x = -offset; x < w + offset; x++)
					{
						var idx = 0;
						for(int i = 0; i < 9; i++)
						{
							var xx = x + area[i].X;
							var yy = y + area[i].Y;
							if (xx < 0 || xx >= w || yy < 0 || yy >= h)
								idx |= outsidePixel << (8 - i);
							else if (map.ContainsKey((xx, yy)))
								idx |= 1 << (8 - i);
						}

						if (iea[idx] == '#')
							newMap.Add((x + offset, y + offset), true);
					}
				}

				outsidePixel ^= 1;
				w += 2 * offset;
				h += 2 * offset;
				map = newMap;

				//print map
				/*var sb = new StringBuilder();
				for (y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						sb.Append(map.ContainsKey((x, y)) ? '#' : '.');
					}
					Console.WriteLine(sb.ToString());
					sb.Clear();
				}*/
			}

			Console.WriteLine("Light pixels: {0}", map.Count);
		}
	}
}
