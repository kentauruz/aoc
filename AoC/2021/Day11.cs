﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_11(bool firstPuzzle)
		{
			var input = @"7147713556
6167733555
5183482118
3885424521
7533644611
3877764863
7636874333
8687188533
7467115265
1626573134";

			var adjacents = new (int X, int Y)[]
			{
				(-1, -1),
				(0, -1),
				(1, -1),
				(-1, 0),
				(1, 0),
				(-1, 1),
				(0, 1),
				(1, 1)
			};

			var cave = input.Where(ch => ch != '\n').Select(ch => ch - '0').ToArray();
			long flashes = 0;
			int step;
			var maxSteps = firstPuzzle ? 100 : int.MaxValue;

			for(step = 0; step < maxSteps; step++)
			{
				var toFlash = new List<(int X, int Y)>();

				//inc everything by 1 a remember positions of octopuses with energy above lvl 9
				for(int i = 0; i < cave.Length; i++)
				{
					if (++cave[i] > 9)
						toFlash.Add((i % 10, i / 10));
				}

				//process flashes
				for(int f = 0; f < toFlash.Count; f++)
				{
					var flash = toFlash[f];

					foreach(var adj in adjacents)
					{
						var x = flash.X + adj.X;
						var y = flash.Y + adj.Y;

						if (x >= 0 && x < 10)
						{
							var idx = x + y * 10;
							if (idx >= 0 && idx < cave.Length && ++cave[idx] == 10) //exclude already flashing ones
								toFlash.Add((x, y));
						}
					}
				}

				//reset flashing ones to zero
				foreach(var ff in toFlash)
				{
					cave[ff.X + ff.Y * 10] = 0;
				}

				flashes += toFlash.Count;

				if (!firstPuzzle && toFlash.Count == cave.Length)
					break;
			}

			if (firstPuzzle)
				Console.WriteLine("Num of flashes after 100 steps is {0}", flashes);
			else
				Console.WriteLine("First party step is {0}", step + 1);
		}
	}
}
