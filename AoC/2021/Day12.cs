﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_12(bool firstPuzzle)
		{
			var input = @"RT-start
bp-sq
em-bp
end-em
to-MW
to-VK
RT-bp
start-MW
to-hr
sq-AR
RT-hr
bp-to
hr-VK
st-VK
sq-end
MW-sq
to-RT
em-er
bp-hr
MW-em
st-bp
to-start
em-st
st-end
VK-sq
hr-st";

			var graph = new Dictionary<string, (int Visits, List<string> Neighbours)>();
			foreach(var line in input.Split('\n'))
			{
				var nodes = line.Split('-');
				var n1 = nodes[0];
				var n2 = nodes[1];
				if (!graph.ContainsKey(n1))
					graph[n1] = (0, new List<string>());
				graph[n1].Neighbours.Add(n2);
				if (!graph.ContainsKey(n2))
					graph[n2] = (0,  new List<string>());
				graph[n2].Neighbours.Add(n1);
			}

			var paths = GetPaths("start", !firstPuzzle, graph);

			Console.WriteLine("Paths: {0}", paths);
		}

		private static int GetPaths(string node, bool allowSmallTwice, Dictionary<string, (int Visits, List<string> Neighbours)> graph)
		{
			if (node == "end")
				return 1;

			var nd = graph[node];
			var visits = nd.Visits + 1;
			graph[node] = (visits, nd.Neighbours);

			var paths = 0;

			foreach(var n in nd.Neighbours)
			{
				var allowTwice = allowSmallTwice;
				var scan = char.IsUpper(n[0]);
				if (!scan && n != "start")
				{
					scan = graph[n].Visits == 0;
					if (allowTwice && !scan)
					{
						scan = true;
						allowTwice = false;
					}
				}
				if (scan)
					paths += GetPaths(n, allowTwice, graph);
			}

			graph[node] = (visits - 1, nd.Neighbours);

			return paths;
		}
	}
}
