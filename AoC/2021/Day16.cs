﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_16(bool firstPuzzle)
		{
			var input = @"A20D790042F1274011955491808B802F1C60B20030327AF2CC248AA800E7CDD726F3D78F4966F571A300BA54D668E2519249265160803EA9DE562A1801204ACE53C954ACE53C94C659BDF318FD1366EF44D96EB11005FB39154E0068A7C3A6B379646C80348A0055E6642B332109B8D6F0F12980452C9D322B28012EC72D51B300426CF70017996DE6C2B2C70C01A04B67B9F9EC8DAFE679D0992A80380104065FA8012805BD380120051E380146006380142004A00E920034C0801CA007B0099420053007144016E28018800CCC8CBB5FE79A3D91E1DC9FB151A1006CC0188970D6109803B1D61344320042615C198C2A014C589D00943096B3CCC081009173D015B004C401C8E10421E8002110BA18C193004A52257E0094BCE1ABB94C2C9005112DFAA5E80292B405927020106BC01494DFA6E329BF4DD273B69E233DB04C435BEF7A0CC00CFCDF31DC6AD20A3002A498CC01D00042229479890200E4438A91700010F88F0EA251802D33FE976802538EF38E2401B84CA05004833529CD2A5BD9DDAC566009CC33E8024200CC528E71F40010A8DF0C61D8002B5076719A5D418034891895CFD320730F739A119CB2EA0072D25E870EA465E189FDC1126AF4B91100A03600A0803713E2FC7D00043A25C3B8A12F89D2E6440242489A7802400086C788FB09C0010C8BB132309005A1400D2CBE7E7F2F9F9F4BB83803B25286DFE628E129EBCB7483C8802F3D0A2542E3004AC0169BD944AFF263361F1B48010496089807100BA54A66675769B1787D230C621EF8B9007893F058A009AE4ED7A5BBDBE05262CEC0002FC7C20082622E0020D0D66A2D04021D5003ED3D396E19A1149054FCA3586BD00020129B0037300042E0CC1184C000874368F70A251D840239798AC8DC9A56F7C6C0E0728015294D9290030B226938A928D0";

			var stream = (Position: 0, Data: input);
			var versionSum = 0;
			var calculator = new Stack<(string Operator, List<long> Operands, (int Type, int Value) Length)>();
			(string Operator, List<long> Operands, (int Type, int Value) Length) current = (null, new List<long>(), (0, 0));

			while (!EndOfStream(stream))
			{
				var version = Read3Bits(ref stream);
				if (EndOfStream(stream))
					break;
				var typeId = Read3Bits(ref stream);

				if (typeId == 4)
				{
					var goOn = 0;
					var numVal = 0L;
					do
					{
						goOn = Read1Bit(ref stream);
						numVal <<= 4;
						numVal |= (long)Read4Bits(ref stream);
					} while (goOn == 1);

					current.Operands.Add(numVal);
				}
				else
				{
					string op;

					if (typeId == 0)
						op = "+";
					else if (typeId == 1)
						op = "*";
					else if (typeId == 2)
						op = "Min";
					else if (typeId == 3)
						op = "Max";
					else if (typeId == 5)
						op = ">";
					else if (typeId == 6)
						op = "<";
					else if (typeId == 7)
						op = "=";
					else
						throw new Exception(string.Format("Invalid packet type ID {0}", typeId));

					var lengthTypeId = Read1Bit(ref stream);
					var val = 0;
					if (lengthTypeId == 0)
						val = Read15Bits(ref stream) + stream.Position;
					else //if (lengthTypeId == 1)
						val = Read11Bits(ref stream);

					calculator.Push(current);
					current = (op, new List<long>(), (lengthTypeId, val));
				}

				//check if we have alread read all sub-packets
				bool allRead;
				do
				{
					allRead = false;
					if (current.Operator != null)
					{
						if (current.Length.Type == 0)   //bits
						{
							allRead = stream.Position == current.Length.Value;
						}
						else //if (current.Length.Type == 1)	//num of packets
						{
							allRead = current.Length.Value == 0;
							current.Length.Value--;
						}

						if (allRead)
						{
							//current packet fully read, resolve its value
							var exprVal = EvaluateExpression(current.Operator, current.Operands);

							//and add it to operands list of parent packet
							current = calculator.Pop();
							current.Operands.Add(exprVal);
						}
					}
				} while (allRead);

				versionSum += version;
			}

			if (firstPuzzle)
				Console.WriteLine("Sum of versions: {0}", versionSum);
			else
				Console.WriteLine("Result of expression: {0}", EvaluateExpression(current.Operator, current.Operands));
		}

		private static bool EndOfStream((int Position, string Data) stream)
		{
			return stream.Position >= stream.Data.Length * 4;
		}
		private static int Read1Bit(ref (int Position, string Data) stream)
		{
			var bit = 0;

			var pos = stream.Position;
			if (pos < stream.Data.Length * 4)
			{
				var idx = pos / 4;
				var bitPos = (3 - (pos % 4));
				bit = (ToDecimal(stream.Data[idx]) >> bitPos) & 1;
				stream.Position++;
			}

			return bit;
		}
		private static int Read3Bits(ref (int Position, string Data) stream)
		{
			var bits = 0;

			bits |= Read1Bit(ref stream) << 2;
			bits |= Read1Bit(ref stream) << 1;
			bits |= Read1Bit(ref stream);

			return bits;
		}
		private static int Read4Bits(ref (int Position, string Data) stream)
		{
			var bits = 0;

			bits |= Read3Bits(ref stream) << 1;
			bits |= Read1Bit(ref stream);

			return bits;
		}
		private static int Read11Bits(ref (int Position, string Data) stream)
		{
			var bits = 0;

			bits |= Read4Bits(ref stream) << 7;
			bits |= Read4Bits(ref stream) << 3;
			bits |= Read3Bits(ref stream);

			return bits;
		}
		private static int Read15Bits(ref (int Position, string Data) stream)
		{
			var bits = 0;

			bits |= Read11Bits(ref stream) << 4;
			bits |= Read4Bits(ref stream);

			return bits;
		}
		private static int ToDecimal(char hex)
		{
			if (hex >= '0' && hex <= '9')
				return hex - '0';
			else
			{
				hex = char.ToUpper(hex);
				if (hex >= 'A' && hex <= 'F')
					return 10 + hex - 'A';
				else
					throw new Exception(string.Format("Not a hexadecimal character {0}", hex));
			}
		}
		private static long EvaluateExpression(string op, List<long> operands)
		{
			var exprVal = 0L;
			if (op == "+")
				exprVal = operands.Sum();
			else if (op == "*")
			{
				exprVal = 1;
				foreach (var o in operands)
					exprVal *= o;
			}
			else if (op == "Min")
				exprVal = operands.Min();
			else if (op == "Max")
				exprVal = operands.Max();
			else if (op == ">" || op == "<" || op == "=")
			{
				if (operands.Count != 2)
					throw new Exception(string.Format("Invalid number of operands {0}. Must be 2.", operands.Count));

				var op1 = operands[0];
				var op2 = operands[1];

				if (op == ">")
					exprVal = (op1 > op2) ? 1 : 0;
				else if (op == "<")
					exprVal = (op1 < op2) ? 1 : 0;
				else if (op == "=")
					exprVal = (op1 == op2) ? 1 : 0;
				//else could not happen
			}
			else if (op == null)
				exprVal = operands[0];
			else
				throw new Exception(string.Format("Invalid operator {0}", op));

			return exprVal;
		}
	}
}
