﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_6(bool firstPuzzle)
		{
			var input = @"1,3,4,1,1,1,1,1,1,1,1,2,2,1,4,2,4,1,1,1,1,1,5,4,1,1,2,1,1,1,1,4,1,1,1,4,4,1,1,1,1,1,1,1,2,4,1,3,1,1,2,1,2,1,1,4,1,1,1,4,3,1,3,1,5,1,1,3,4,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,5,2,5,5,3,2,1,5,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,5,1,1,1,1,5,1,1,1,1,1,4,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,3,1,2,4,1,5,5,1,1,5,3,4,4,4,1,1,1,2,1,1,1,1,1,1,2,1,1,1,1,1,1,5,3,1,4,1,1,2,2,1,2,2,5,1,1,1,2,1,1,1,1,3,4,5,1,2,1,1,1,1,1,5,2,1,1,1,1,1,1,5,1,1,1,1,1,1,1,5,1,4,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,5,4,5,1,1,1,1,1,1,1,5,1,1,3,1,1,1,3,1,4,2,1,5,1,3,5,5,2,1,3,1,1,1,1,1,3,1,3,1,1,2,4,3,1,4,2,2,1,1,1,1,1,1,1,5,2,1,1,1,2";

			var init = input.Split(',');
			var gen = new long[9];

			//init
			foreach(var x in init)
			{
				var t = long.Parse(x);
				gen[t]++;
			}
			var days = firstPuzzle ? 80 : 256;
			for(int i = 0; i < days; i++)
			{
				var newGen = new long[9];

				var toClone = gen[0];
				newGen[8] += toClone;
				newGen[6] += toClone;

				for(int g = 1; g < 9; g++)
				{
					newGen[g - 1] += gen[g];
				}

				Array.Copy(newGen, gen, 9);
			}

			var sum = gen.Sum();

			Console.WriteLine("After 80 days, there are {0} fish", sum);
		}
	}
}
