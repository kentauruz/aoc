﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_21(bool firstPuzzle)
		{
			var pos = new int[] { 8, 3 };
			//var pos = new int[] { 4, 8 };

			if (firstPuzzle)
			{
				var score = new int[2];
				var dice = 1;
				var rollCount = 0;

				while (score[0] < 1000 && score[1] < 1000)
				{
					var i = 0;
					for (; i < 2; i++)
					{
						pos[i] = ((pos[i] + dice * 3 + 2) % 10) + 1;    //2 = 3 - 1
						score[i] += pos[i];
						rollCount += 3;
						if (score[i] >= 1000)
							break;
						dice += 3;
					}
					if (i < 2 && score[i] >= 1000)
						break;
				}

				var res = rollCount * score.Min();
				Console.WriteLine("Result: {0}", res);
			}
			else
			{
				var wins = new long[2];
				var t = DateTime.Now;
				foreach (var s in RollsFrequency.Keys)
				{
					Roll(0, s, pos, new int[2], wins, 0, new List<Dictionary<int, Dictionary<int, long>>>());
				}
				Console.WriteLine("Time: {0}", (DateTime.Now - t).TotalSeconds);

				Console.WriteLine("More wins: {0}", wins.Max());
			}
		}

		private static Dictionary<int, int> RollsFrequency = new Dictionary<int, int>()
		{
			{ 3, 1 },
			{ 4, 3 },
			{ 5, 6 },
			{ 6, 7 },
			{ 7, 6 },
			{ 8, 3 },
			{ 9, 1 }

			//{ 1, 3 },
			//{ 3, 1 },
		};

		private static void Roll(int player, int rollSum, int[] pos, int[] score, long[] wins, int round, List<Dictionary<int, Dictionary<int, long>>> cachedWins)
		{
			//is number of wins in this round on specified position for specified roll cached?
			Dictionary<int, Dictionary<int, long>> cachedPos;
			if (round < cachedWins.Count)
				cachedPos = cachedWins[round];
			else
			{
				cachedPos = new Dictionary<int, Dictionary<int, long>>();
				cachedWins.Add(cachedPos);
			}
			Dictionary<int, long> cachedRollWins;
			if (!cachedPos.TryGetValue(pos[player], out cachedRollWins))
			{
				cachedRollWins = new Dictionary<int, long>();
				cachedPos.Add(pos[player], cachedRollWins);
			}
			//TODO (Never) - fix the cache ;) but result is computed in 7s anyway :D
			//if (cachedRollWins.ContainsKey(rollSum))
			//{
			//	//if so, just use it
			//	wins[player] += cachedRollWins[rollSum]; //* RollsFrequency[rollSum];
			//	return;
			//}

			//no, determine if current player wins
			var newPos = ((pos[player] - 1 + rollSum) % 10) + 1;
			var newScore = score[player] + newPos;
			if (newScore >= 21)
			{
				//if so, end this roll
				var wnss = RollsFrequency[rollSum];
				wins[player] += wnss;
				cachedRollWins[rollSum] = wnss;
				return;
			}

			//no, next player
			var nextPos = new int[2];
			var nextScore = new int[2];
			nextPos[player] = newPos;
			nextScore[player] = newScore;
			var nextPlayer = (player + 1) % 2;
			nextPos[nextPlayer] = pos[nextPlayer];
			nextScore[nextPlayer] = score[nextPlayer];
			round++;
			var nextWins = new long[2];
			foreach (var s in RollsFrequency.Keys)
			{
				Roll(nextPlayer, s, nextPos, nextScore, nextWins, round, cachedWins);
			}

			//update computed wins for next player
			var wns = nextWins[nextPlayer] * RollsFrequency[rollSum];
			wins[nextPlayer] += wns;// * RollsFrequency[rollSum];
			cachedRollWins[rollSum] = wns;

			wins[player] += nextWins[player] * RollsFrequency[rollSum];
		}
	}
}
