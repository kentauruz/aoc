﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2021_14(bool firstPuzzle)
		{
			var input = @"SCVHKHVSHPVCNBKBPVHV";
			var rulesInput = @"SB -> B
HH -> P
VF -> N
BS -> S
NC -> C
BF -> H
BN -> H
SP -> H
BK -> H
FF -> N
VN -> B
FN -> C
FS -> S
PP -> F
ON -> H
FV -> F
KO -> F
PK -> H
VB -> S
HS -> B
NV -> O
PN -> S
VH -> B
OS -> P
BP -> H
OV -> B
HK -> S
NN -> K
SV -> C
PB -> F
SK -> F
FB -> S
NB -> K
HF -> P
FK -> K
KV -> P
PV -> F
BC -> S
FO -> N
HC -> F
CP -> B
KK -> F
PC -> S
HN -> O
SH -> H
CK -> P
CO -> F
HP -> K
PS -> C
KP -> F
OF -> K
KS -> F
NO -> V
CB -> K
NF -> N
SF -> F
SC -> P
FC -> V
BV -> B
SS -> O
KC -> K
FH -> C
OP -> C
CF -> K
VO -> V
VK -> H
KH -> O
NP -> V
NH -> O
NS -> V
BH -> C
CH -> S
CC -> F
CS -> P
SN -> F
BO -> S
NK -> S
OO -> P
VV -> F
FP -> V
OK -> C
SO -> H
KN -> P
HO -> O
PO -> H
VS -> N
PF -> N
CV -> F
BB -> H
VC -> H
HV -> B
CN -> S
OH -> K
KF -> K
HB -> S
OC -> H
KB -> P
OB -> C
VP -> C
PH -> K";

			var rulesLines = rulesInput.Split('\n');
			var rules = new Dictionary<string, string>();
			foreach(var l in rulesLines)
			{
				var p = l.Split(" -> ");
				rules.Add(p[0], p[1]);
			}

			var counts = new Dictionary<char, long>();
			var maxSteps = firstPuzzle ? 10 : 40;

			var countedPairs = new List<Dictionary<string, Dictionary<char, long>>>();
			for (int i = 0; i < maxSteps; i++)
				countedPairs.Add(new Dictionary<string, Dictionary<char, long>>());
			var t = DateTime.Now;
			for (int i = 0; i < input.Length - 1; i++)
			{
				Console.WriteLine("Process {0}{1}", input[i], input[i + 1]);
				ProcessPair(input[i], input[i + 1], 0, rules, counts, countedPairs, maxSteps);

				AddChar(input[i], 1, counts);
			}
			AddChar(input[input.Length - 1], 1, counts);
			Console.WriteLine("Time: {0}", (DateTime.Now - t).TotalSeconds);
			var cnts = counts.Values.ToList();
			cnts.Sort();
			var result = cnts[cnts.Count - 1] - cnts[0];
			Console.WriteLine("Result {0}", result);
		}

		private static void ProcessPair(char p1, char p2, int step, Dictionary<string, string> rules,
			Dictionary<char, long> counts, List<Dictionary<string, Dictionary<char, long>>> countedPairs, int maxSteps)
		{
			var pair = p1.ToString() + p2;
			var insert = rules[pair][0];
			if (!countedPairs[step].ContainsKey(pair))
			{
				var pairCounts = new Dictionary<char, long>();
				AddChar(insert, 1, pairCounts);
				if (step < maxSteps - 1)
				{
					ProcessPair(p1, insert, step + 1, rules, pairCounts, countedPairs, maxSteps);
					ProcessPair(insert, p2, step + 1, rules, pairCounts, countedPairs, maxSteps);
				}
				countedPairs[step][pair] = pairCounts;
			}

			foreach (var kp in countedPairs[step][pair])
			{
				AddChar(kp.Key, kp.Value, counts);
			}
		}

		private static void AddChar(char ch, long count, Dictionary<char, long> counts)
		{
			if (counts.ContainsKey(ch))
				counts[ch] += count;
			else
				counts[ch] = count;
		}
	}
}
