﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace AoC
{
	partial class Program
	{
		static void Day_2020_13(bool firstPuzzle)
		{
			var availTime = 1000507;
			var input = @"29,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,631,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,19,x,x,x,23,x,x,x,x,x,x,x,383,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,17";

			if (firstPuzzle)
			{
				var buses = input.Split(",").Where(b => b != "x").Select(b => int.Parse(b));
				var bestBus = 0;
				var waitTime = int.MaxValue;

				foreach (var bus in buses)
				{
					var wait = bus - (availTime % bus);
					if (wait < waitTime)
					{
						waitTime = wait;
						bestBus = bus;
					}
				}

				Console.WriteLine("Result {0} * {1} = {2}", bestBus, waitTime, bestBus * waitTime);
			}
			else
			{
				Console.WriteLine("------------------");

				var t = 0;
				var buses = input.Split(",").Select<string, (int Id, int Offset)>(b => (b == "x") ? (-1, t++) : (int.Parse(b), t++)).Where(b => b.Id != -1).ToList();

				long time = 0;
				long lcm = buses[0].Id;
				for(int i = 1; i < buses.Count; i++)
				{
					var bus = buses[i];

					for(int n = 1; n < int.MaxValue; n++)
					{
						var b = time + lcm * n + bus.Offset;
						if ((b % bus.Id) == 0)
						{
							Console.WriteLine("Bus {0}: {1}", bus, b);

							lcm = LCM(new long[] { lcm, bus.Id });
							time = b - bus.Offset;

							for(int j = 0; j <= i; j++)
							{
								var bbus = buses[j];
								Console.Write("Bus {0}: {1} ({2}), ", bbus, time % bbus.Id, (((bbus.Offset / bbus.Id) + 1) * bbus.Id) - bbus.Offset);
							}
							Console.WriteLine();

							break;
						}
					}
				}

				Console.WriteLine(time);

				Console.WriteLine("Check (time + offset) % id == 0");
				foreach (var bus in buses)
				{
					Console.WriteLine("({0} + {1}) % {2} = {3}", time, bus.Offset, bus.Id, (time + bus.Offset) % bus.Id);
				}
			}
		}
	}
}
