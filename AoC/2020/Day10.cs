﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2020_10(bool firstPuzzle)
		{
			var input = @"48
171
156
51
26
6
80
62
65
82
130
97
49
31
142
83
75
20
154
119
56
114
92
33
140
74
118
1
96
44
128
134
121
64
158
27
17
101
59
12
89
88
145
167
11
3
39
43
105
16
170
63
111
2
108
21
146
77
45
52
32
127
147
76
58
37
86
129
57
133
120
163
138
161
139
71
9
141
168
164
124
157
95
25
38
69
87
155
135
15
102
70
34
42
24
50
68
169
10
55
117
30
81
151
100
162
148";

			var adapters = input.Split("\n").Select(i => int.Parse(i)).ToList();
			adapters.Sort();

			if (firstPuzzle)
			{
				var diffs = new int[3];
				diffs[2]++;

				for (int i = 0; i < adapters.Count; i++)
				{
					var diff = (i == 0) ? adapters[i] : adapters[i] - adapters[i - 1];
					if (diff < 1 || diff > 3)
					{
						Console.WriteLine("What the hell {0}-{1}={2}", adapters[i], adapters[i - 1], diff);
						return;
					}

					diffs[diff - 1]++;
				}

				Console.WriteLine("{0} {1} {2} {3}", diffs[0], diffs[1], diffs[2], diffs[0] * diffs[2]);
			}
			else
			{
				var graph = new List<int>();
				graph.Add(0);
				graph.AddRange(adapters);
				graph.Add(graph[graph.Count - 1] + 3);

				var fre = new int[graph.Count];
				var v = new List<int>[graph.Count];
				for(int i = 0; i < v.Length; i++)
					v[i] = new List<int>();

				for(int i = 0; i < graph.Count - 1; i++)
				{
					for(int j = i + 1; j < graph.Count; j++)
					{
						if (graph[j] - graph[i] < 4)
							Day10_AddEdge(i, j, fre, v);
						else
							break;
					}
				}

				var count = Day10_NumberofPaths(0, graph.Count - 1, graph.Count, fre, v);

				Console.WriteLine("Distinct arrangements {0}", count);
			}
		}

		// function to add edge in graph 
		private static void Day10_AddEdge(int a, int b, int[] fre, List<int>[] v)
		{
			// there is path from a to b. 
			v[a].Add(b);
			fre[b]++;
		}
		// function to make topological sorting 
		private static List<int> Day10_TopologicalSorting(int[] fre, int n, List<int>[] v)
		{
			var q = new Queue<int>();

			// insert all vertices which don't have any parent.
			for(int i = 0; i < n; i++)
			{
				if (fre[i] == 0)
					q.Enqueue(i);
			}

			var l = new List<int>();

			// using kahn's algorithm for topological sorting
			while (q.Count > 0)
			{
				var u = q.Dequeue();

				// insert front element of queue to vector 
				l.Add(u);

				// go through all it's childs 
				for (int i = 0; i < v[u].Count; i++)
				{
					fre[v[u][i]]--;

					// whenever the freqency is zero then add this vertex to queue. 
					if (fre[v[u][i]] == 0)
						q.Enqueue(v[u][i]);
				}
			}
			return l;
		}
		// Function that returns the number of paths 
		private static long Day10_NumberofPaths(int source, int destination, int n, int[] fre, List<int>[] v)
		{
			// make topological sorting 
			var s = Day10_TopologicalSorting(fre, n, v);

			// to store required answer. 
			var dp = new long[n];

			// answer from destination to destination is 1. 
			dp[destination] = 1;

			// traverse in reverse order 
			for (int i = s.Count - 1; i >= 0; i--)
			{
				for (int j = 0; j < v[s[i]].Count; j++)
				{
					dp[s[i]] += dp[v[s[i]][j]];
				}
			}

			return dp[0];
		}
	}
}
