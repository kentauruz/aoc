﻿using System;
using System.Collections.Generic;

namespace AoC
{
	partial class Program
	{
		static void Day_2020_15(bool firstPuzzle)
		{
			var input = new int[] { 13, 16, 0, 12, 15, 1 };
			var spoken = new Dictionary<int, int>();

			var next = -1;

			for(int i = 0; i < ((firstPuzzle ? 2020 : 30000000) - 1); i++)
			{
				var n = (i < input.Length) ? input[i] : next;

				int t;
				if (spoken.TryGetValue(n, out t))
					next = i - t;
				else
					next = 0;

				spoken[n] = i;
			}

			Console.WriteLine("2020th number is {0}", next);
		}
	}
}
