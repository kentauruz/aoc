﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2022_10(bool firstPuzzle)
		{
			var input = @"noop
addx 25
addx -5
addx -14
addx 4
noop
addx 2
addx 3
noop
noop
noop
noop
addx 3
addx 5
addx 2
noop
noop
addx 5
noop
noop
noop
addx 1
addx 2
addx 5
addx -40
addx 5
noop
addx 26
addx -20
addx -3
addx 2
noop
addx -4
addx 9
addx 5
addx 2
addx 11
addx -10
addx 2
addx 5
addx 2
addx 5
noop
noop
noop
addx -31
addx 32
addx -37
addx 1
addx 8
addx 13
addx -15
addx 4
noop
addx 5
noop
addx 3
addx -2
addx 4
addx 1
addx 4
addx -14
addx 15
addx 4
noop
noop
noop
addx 3
addx 5
addx -40
noop
addx 5
addx 8
addx -3
noop
addx 2
addx 9
addx -4
noop
noop
noop
noop
addx 5
addx -9
addx 10
addx 4
noop
noop
addx 5
addx -19
addx 24
addx -2
addx 5
addx -40
addx 22
addx -19
addx 2
addx 5
addx 2
addx 5
noop
noop
addx -2
addx 2
addx 5
addx 3
noop
addx 2
addx 2
addx 3
addx -2
addx 10
addx -3
addx 3
noop
addx -40
addx 2
addx 11
addx -5
addx -1
noop
addx 3
addx 7
noop
addx -2
addx 5
addx 2
addx 3
noop
addx 2
addx 6
addx -5
addx 2
addx -18
addx 26
addx -1
noop
noop
noop
noop";

			var cycle = 1;
			var x = 1;
			var signal = 0;

			var screen = new char[40 * 6];
			var pos = 0;

			var program = input.Split('\n');

			foreach(var line in program)
			{
				if (IsCheckPoint(cycle))
					signal += cycle * x;

				pos = (cycle - 1) % 40;
				screen[cycle - 1] = (pos >= (x - 1) && pos <= (x + 1)) ? '#' : '.';

				cycle++;

				var instr = line.Substring(0, 4);
				if (instr == "noop")
				{
					//no op
				}
				else if (instr == "addx")
				{
					if (IsCheckPoint(cycle))
						signal += cycle * x;

					pos = (cycle - 1) % 40;
					screen[cycle - 1] = (pos >= (x - 1) && pos <= (x + 1)) ? '#' : '.';

					cycle++;
					x += int.Parse(line.Substring(5));
				}
				else
				{
					throw new Exception(String.Format("Invalid instruction: {0}", line));
				}
			}

			//dont forget to count last instruction
			if (IsCheckPoint(cycle))
				signal += cycle * x;

			if (firstPuzzle)
			{
				Console.WriteLine("Strength: {0}", signal);
			}
			else
			{
				for(int r = 0; r < 6; r++)
				{
					for(int c = 0; c < 40; c++)
						Console.Write(screen[c + r * 40]);
					Console.WriteLine();
				}
			}
		}

		static bool IsCheckPoint(int cycle)
		{
			return cycle == 20 || cycle == 60 || cycle == 100 || cycle == 140 || cycle == 180 || cycle == 220;
		}
	}
}
