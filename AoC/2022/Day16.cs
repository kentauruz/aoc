﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		class Valve
		{
			public string Name;
			public int Rate;
			public List<string> Tunnels;
			public Dictionary<string, int> ShortestPaths;

			public Valve(string name, int rate)
			{
				this.Name = name;
				this.Rate = rate;
				this.ShortestPaths = new Dictionary<string, int>();
			}

			public override string ToString()
			{
				var str = String.Format("{0} {1}", this.Name, this.Rate);
				if (this.Tunnels != null)
				{
					foreach (var t in this.Tunnels)
						str += " " + t;
				}
				return str;
			}
		}

		static void Day_2022_16(bool firstPuzzle)
		{
			var input = @"Valve VR has flow rate=11; tunnels lead to valves LH, KV, BP
Valve UV has flow rate=0; tunnels lead to valves GH, RO
Valve OH has flow rate=0; tunnels lead to valves AJ, NY
Valve GD has flow rate=0; tunnels lead to valves TX, PW
Valve NS has flow rate=0; tunnels lead to valves AJ, AA
Valve KZ has flow rate=18; tunnels lead to valves KO, VK, PJ
Valve AH has flow rate=0; tunnels lead to valves ZP, DI
Valve SA has flow rate=0; tunnels lead to valves VG, JF
Valve VK has flow rate=0; tunnels lead to valves RO, KZ
Valve GB has flow rate=0; tunnels lead to valves XH, AA
Valve AJ has flow rate=6; tunnels lead to valves IC, OH, ZR, NS, EM
Valve PJ has flow rate=0; tunnels lead to valves KZ, SP
Valve KO has flow rate=0; tunnels lead to valves KZ, LE
Valve AA has flow rate=0; tunnels lead to valves TW, GB, TI, NS, UL
Valve TW has flow rate=0; tunnels lead to valves TU, AA
Valve VG has flow rate=25; tunnel leads to valve SA
Valve BP has flow rate=0; tunnels lead to valves RO, VR
Valve XH has flow rate=0; tunnels lead to valves GB, RI
Valve TX has flow rate=0; tunnels lead to valves RI, GD
Valve IR has flow rate=10; tunnels lead to valves TN, NY, JF
Valve TU has flow rate=0; tunnels lead to valves JD, TW
Valve KC has flow rate=0; tunnels lead to valves SP, RO
Valve LN has flow rate=0; tunnels lead to valves EM, RI
Valve HD has flow rate=0; tunnels lead to valves FE, SC
Valve KE has flow rate=0; tunnels lead to valves OM, RI
Valve VY has flow rate=0; tunnels lead to valves PW, BS
Valve LH has flow rate=0; tunnels lead to valves OM, VR
Valve EM has flow rate=0; tunnels lead to valves AJ, LN
Valve SO has flow rate=22; tunnels lead to valves ZP, FE
Valve EC has flow rate=0; tunnels lead to valves OM, UL
Valve KV has flow rate=0; tunnels lead to valves SP, VR
Valve FE has flow rate=0; tunnels lead to valves SO, HD
Valve TI has flow rate=0; tunnels lead to valves AA, PW
Valve SC has flow rate=14; tunnel leads to valve HD
Valve ZP has flow rate=0; tunnels lead to valves SO, AH
Valve RO has flow rate=19; tunnels lead to valves UV, BP, VK, KC
Valve ZR has flow rate=0; tunnels lead to valves OM, AJ
Valve JL has flow rate=21; tunnels lead to valves GN, TN
Valve PW has flow rate=9; tunnels lead to valves TI, GN, VY, GD, IC
Valve UL has flow rate=0; tunnels lead to valves EC, AA
Valve GN has flow rate=0; tunnels lead to valves JL, PW
Valve TN has flow rate=0; tunnels lead to valves JL, IR
Valve NV has flow rate=0; tunnels lead to valves RI, JD
Valve DI has flow rate=23; tunnels lead to valves LE, AH
Valve IC has flow rate=0; tunnels lead to valves PW, AJ
Valve JF has flow rate=0; tunnels lead to valves SA, IR
Valve LE has flow rate=0; tunnels lead to valves DI, KO
Valve BS has flow rate=0; tunnels lead to valves JD, VY
Valve JD has flow rate=15; tunnels lead to valves NV, TU, BS
Valve SP has flow rate=24; tunnels lead to valves KC, KV, PJ
Valve NY has flow rate=0; tunnels lead to valves IR, OH
Valve OM has flow rate=7; tunnels lead to valves EC, GH, KE, ZR, LH
Valve GH has flow rate=0; tunnels lead to valves OM, UV
Valve RI has flow rate=3; tunnels lead to valves NV, KE, LN, XH, TX";

			var input3 = @"Valve YK has flow rate=0; tunnels lead to valves GL, FT
Valve QA has flow rate=0; tunnels lead to valves JX, FD
Valve LN has flow rate=0; tunnels lead to valves FD, FG
Valve AU has flow rate=0; tunnels lead to valves BD, PQ
Valve MM has flow rate=0; tunnels lead to valves UL, AA
Valve JX has flow rate=0; tunnels lead to valves QA, NZ
Valve CV has flow rate=0; tunnels lead to valves UP, QW
Valve UZ has flow rate=0; tunnels lead to valves FG, NZ
Valve BP has flow rate=0; tunnels lead to valves TI, DX
Valve NS has flow rate=0; tunnels lead to valves ZL, CW
Valve CO has flow rate=0; tunnels lead to valves BD, AT
Valve RZ has flow rate=0; tunnels lead to valves AA, ZO
Valve PQ has flow rate=0; tunnels lead to valves ML, AU
Valve CW has flow rate=7; tunnels lead to valves UL, PH, OF, NS, GT
Valve FG has flow rate=14; tunnels lead to valves SO, JR, IN, LN, UZ
Valve EZ has flow rate=0; tunnels lead to valves UP, QP
Valve GN has flow rate=0; tunnels lead to valves VQ, CH
Valve QW has flow rate=6; tunnels lead to valves CV, PF, KH, UY, TI
Valve UL has flow rate=0; tunnels lead to valves MM, CW
Valve VQ has flow rate=12; tunnels lead to valves GN, LC
Valve FT has flow rate=0; tunnels lead to valves SG, YK
Valve SG has flow rate=21; tunnels lead to valves FT, LC, NO, QX
Valve BD has flow rate=23; tunnels lead to valves CO, AU, AB
Valve AB has flow rate=0; tunnels lead to valves BD, QX
Valve QP has flow rate=0; tunnels lead to valves AD, EZ
Valve OF has flow rate=0; tunnels lead to valves DX, CW
Valve AA has flow rate=0; tunnels lead to valves QL, RZ, SO, MM, HW
Valve RQ has flow rate=0; tunnels lead to valves GL, QG
Valve ZL has flow rate=0; tunnels lead to valves NS, FD
Valve KH has flow rate=0; tunnels lead to valves GT, QW
Valve JR has flow rate=0; tunnels lead to valves FG, PH
Valve PH has flow rate=0; tunnels lead to valves CW, JR
Valve LC has flow rate=0; tunnels lead to valves VQ, SG
Valve FD has flow rate=20; tunnels lead to valves LN, QA, ZL
Valve NZ has flow rate=15; tunnels lead to valves UZ, JX
Valve ML has flow rate=22; tunnels lead to valves OW, PQ, NO
Valve PF has flow rate=0; tunnels lead to valves QW, CH
Valve UP has flow rate=19; tunnels lead to valves RY, CV, EZ
Valve VM has flow rate=0; tunnels lead to valves RY, CH
Valve DX has flow rate=3; tunnels lead to valves BO, QL, BP, OF, QG
Valve QL has flow rate=0; tunnels lead to valves AA, DX
Valve HW has flow rate=0; tunnels lead to valves UY, AA
Valve GL has flow rate=8; tunnels lead to valves YK, RQ
Valve QG has flow rate=0; tunnels lead to valves DX, RQ
Valve IN has flow rate=0; tunnels lead to valves FG, BO
Valve NO has flow rate=0; tunnels lead to valves SG, ML
Valve SO has flow rate=0; tunnels lead to valves FG, AA
Valve RY has flow rate=0; tunnels lead to valves UP, VM
Valve CH has flow rate=13; tunnels lead to valves GN, VM, PF, ZO
Valve AD has flow rate=17; tunnel leads to valve QP
Valve TI has flow rate=0; tunnels lead to valves BP, QW
Valve UY has flow rate=0; tunnels lead to valves HW, QW
Valve AT has flow rate=24; tunnels lead to valves OW, CO
Valve GT has flow rate=0; tunnels lead to valves CW, KH
Valve ZO has flow rate=0; tunnels lead to valves RZ, CH
Valve QX has flow rate=0; tunnels lead to valves AB, SG
Valve BO has flow rate=0; tunnels lead to valves IN, DX
Valve OW has flow rate=0; tunnels lead to valves AT, ML";

			var input2 = @"Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II";

			var lines = input.Split('\n');
			var valves = new Dictionary<string, Valve>();
			var valvesOfInterest = new List<string>();

			foreach(var line in lines)
			{
				var p = line.Split(' ');
				var name = p[1];
				var rate = int.Parse(p[4].Substring(5, p[4].Length - 6));
				var tunnels = new List<string>();
				for (int i = 9; i < p.Length; i++)
					tunnels.Add(p[i].Substring(0, 2));

				var valve = new Valve(name, rate) { Tunnels = tunnels };
				valves.Add(name, valve);

				if (rate > 0)
					valvesOfInterest.Add(name);
			}

			//precompute shortest paths
			foreach(var v in valves)
			{
				v.Value.ShortestPaths = GetShortestPaths(v.Value.Name, valves);
			}

			var maxTime = firstPuzzle ? 30 : 26;
			var maxPressure = int.MinValue;
			string[] maxPath;

			//H - human, E - elepahnt
			var q = new List<(string CurrentValveH, string CurrentValveE, int Pressure, int TimeH, int TimeE, HashSet<string> Opened)>();
			q.Add(("AA", "AA", 0, 0, 0, new HashSet<string>()));

			while(q.Count > 0)
			{
				var n = q[0];
				q.RemoveAt(0);

				var nameH = n.CurrentValveH;
				var nameE = n.CurrentValveE;
				var pressure = n.Pressure;
				var timeH = n.TimeH;
				var timeE = n.TimeE;
				var opened = n.Opened;

				if (pressure > maxPressure)
				{
					maxPressure = pressure;
					maxPath = opened.ToArray();
				}
				
				var valveH = valves[nameH];

				if (firstPuzzle)
				{
					foreach (var ivH in valvesOfInterest)
					{
						//find all not already opened valves with rate > 0
						if (!opened.Contains(ivH))
						{
							var nt = timeH + valveH.ShortestPaths[ivH] + 1;    //1 - time to open
							if (nt < maxTime)
							{
								var np = pressure + (maxTime - nt) * valves[ivH].Rate;
								var no = new HashSet<string>(opened);
								no.Add(ivH);
								q.Add((ivH, "AA", np, nt, 0, no));
							}
						}
					}
				}
				else
				{
					/*var valveE = valves[nameE];

					foreach (var ivH in valvesOfInterest)
					{
						foreach (var ivE in valvesOfInterest)
						{
							//find all not already opened valves with rate > 0
							if (ivH != ivE && !opened.Contains(ivH) && !opened.Contains(ivE))
							{
								var nt = timeH + valveH.ShortestPaths[iv] + 1;    //1 - time to open
								if (nt < maxTime)
								{
									var np = pressure + (maxTime - nt) * valves[iv].Rate;
									var no = new HashSet<string>(opened);
									no.Add(iv);
									q.Add((iv, "AA", np, nt, 0, no));
								}
							}
						}
					}*/
				}
			}

			Console.WriteLine("Max pressure {0}", maxPressure);
		}

		static Dictionary<string, int> GetShortestPaths(string fromName, Dictionary<string, Valve> valves)
		{
			var distances = new Dictionary<string, int>() { { valves[fromName].Name, 0 } };

			var q = new List<string>();
			q.Add(fromName);
			while (q.Count > 0 && distances.Count < valves.Count)
			{
				var v = valves[q[0]];
				q.RemoveAt(0);

				foreach (var t in v.Tunnels)
				{
					if (!distances.ContainsKey(t))
					{
						distances.Add(t, distances[v.Name] + 1);
						q.Add(t);
					}
				}
			}

			return distances;
		}
	}
}
