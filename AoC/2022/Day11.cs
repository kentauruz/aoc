﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		class Item
		{
			public long Value;
			public string Path;

			public Item(long val, int index)
			{
				this.Value = val;
				this.Path = string.Format("{0}({1})", val, index);
			}
		}

		class Monkey
		{
			public List<Item> Items;
			public bool Operator;   //false +, true *
			public long? Operand2;
			public long Test;
			public int FalseIndex;
			public int TrueIndex;
			public int Inspections;
		}

		static void Day_2022_11(bool firstPuzzle)
		{
			var input = @"Monkey 0:
  Starting items: 63, 84, 80, 83, 84, 53, 88, 72
  Operation: new = old * 11
  Test: divisible by 13
    If true: throw to monkey 4
    If false: throw to monkey 7

Monkey 1:
  Starting items: 67, 56, 92, 88, 84
  Operation: new = old + 4
  Test: divisible by 11
    If true: throw to monkey 5
    If false: throw to monkey 3

Monkey 2:
  Starting items: 52
  Operation: new = old * old
  Test: divisible by 2
    If true: throw to monkey 3
    If false: throw to monkey 1

Monkey 3:
  Starting items: 59, 53, 60, 92, 69, 72
  Operation: new = old + 2
  Test: divisible by 5
    If true: throw to monkey 5
    If false: throw to monkey 6

Monkey 4:
  Starting items: 61, 52, 55, 61
  Operation: new = old + 3
  Test: divisible by 7
    If true: throw to monkey 7
    If false: throw to monkey 2

Monkey 5:
  Starting items: 79, 53
  Operation: new = old + 1
  Test: divisible by 3
    If true: throw to monkey 0
    If false: throw to monkey 6

Monkey 6:
  Starting items: 59, 86, 67, 95, 92, 77, 91
  Operation: new = old + 5
  Test: divisible by 19
    If true: throw to monkey 4
    If false: throw to monkey 0

Monkey 7:
  Starting items: 58, 83, 89
  Operation: new = old * 19
  Test: divisible by 17
    If true: throw to monkey 2
    If false: throw to monkey 1";

			/*var input2 = @"Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";*/

			var lines = input.Split('\n');
			var monkeys = new List<Monkey>();

			Monkey m = null;
			var si = "  Starting items: ";
			var op = "  Operation: new = old ";
			var te = "  Test: divisible by ";
			var tr = "    If true: throw to monkey ";
			var fa = "    If false: throw to monkey ";
			foreach (var l in lines)
			{
				if (l.StartsWith("Monkey"))
				{
					m = new Monkey();
					monkeys.Add(m);
				}
				else if (l.StartsWith(si))
				{
					m.Items = new List<Item>();
					var items = l.Substring(si.Length).Split(", ");
					foreach (var i in items)
						m.Items.Add(new Item(int.Parse(i), monkeys.Count - 1));
				}
				else if (l.StartsWith(op))
				{
					var o = l.Substring(op.Length).Split(' ');
					m.Operator = o[0] == "*";
					if (o[1] != "old")
						m.Operand2 = int.Parse(o[1]);
				}
				else if (l.StartsWith(te))
				{
					m.Test = int.Parse(l.Substring(te.Length));
				}
				else if (l.StartsWith(tr))
				{
					m.TrueIndex = int.Parse(l.Substring(tr.Length));
				}
				else if (l.StartsWith(fa))
				{
					m.FalseIndex = int.Parse(l.Substring(fa.Length));
				}
			}

			//var rounds = firstPuzzle ? 20 : 10000;
			var rounds = 20;
			var div = firstPuzzle ? 3 : 1;
			for(int round = 0; round < rounds; round++)
			{
				foreach(var mo in monkeys)
				{
					while(mo.Items.Count > 0)
					{
						var itm = mo.Items[0];
						mo.Items.RemoveAt(0);
						var val = mo.Operand2.HasValue ? mo.Operand2.Value : itm.Value;
						if (mo.Operator)    //*
						{
							itm.Value *= val;
							itm.Path += String.Format("*{0}", val);
						}
						else //+
						{
							itm.Value += val;
							itm.Path += String.Format("+{0}", val);
						}
						itm.Value /= div;
						var idx = ((itm.Value % mo.Test) == 0) ? mo.TrueIndex : mo.FalseIndex;
						itm.Path += String.Format("({0})", idx);
						monkeys[idx].Items.Add(itm);

						mo.Inspections++;
					}
				}
			}

			monkeys.Sort((m1, m2) => m2.Inspections - m1.Inspections);
			var mb = (long)monkeys[0].Inspections * (long)monkeys[1].Inspections;

			Console.WriteLine("Monkey bussiness: {0}", mb);
		}
	}
}
