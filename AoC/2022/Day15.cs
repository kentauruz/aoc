﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AoC
{
	partial class Program
	{
		class SB
		{
			public int SX;
			public int SY;
			public int BX;
			public int BY;
			public int Top;
			public int Bottom;
			public int Distance;

			public SB(int sx, int sy, int bx, int by)
			{
				this.SX = sx;
				this.SY = sy;
				this.BX = bx;
				this.BY = by;

				this.Distance = Math.Abs(sx - bx) + Math.Abs(sy - by);
				this.Top = sy - this.Distance;
				this.Bottom = sy + this.Distance;
			}

			public override string ToString()
			{
				return String.Format("SX: {0}, SY: {1}, BX: {2}, BY: {3}", this.SX, this.SY, this.BX, this.BY);
			}
		}

		static void Day_2022_15(bool firstPuzzle)
		{
			var input = @"Sensor at x=3842919, y=126080: closest beacon is at x=3943893, y=1918172
Sensor at x=406527, y=2094318: closest beacon is at x=-1066, y=1333278
Sensor at x=2208821, y=3683408: closest beacon is at x=2914373, y=3062268
Sensor at x=39441, y=1251806: closest beacon is at x=-1066, y=1333278
Sensor at x=3093352, y=2404566: closest beacon is at x=2810772, y=2699609
Sensor at x=3645473, y=2234498: closest beacon is at x=3943893, y=1918172
Sensor at x=3645012, y=2995540: closest beacon is at x=4001806, y=2787325
Sensor at x=18039, y=3083937: closest beacon is at x=103421, y=3007511
Sensor at x=2375680, y=551123: closest beacon is at x=2761373, y=2000000
Sensor at x=776553, y=123250: closest beacon is at x=-1066, y=1333278
Sensor at x=2884996, y=2022644: closest beacon is at x=2761373, y=2000000
Sensor at x=1886537, y=2659379: closest beacon is at x=2810772, y=2699609
Sensor at x=3980015, y=3987237: closest beacon is at x=3844688, y=3570059
Sensor at x=3426483, y=3353349: closest beacon is at x=3844688, y=3570059
Sensor at x=999596, y=1165648: closest beacon is at x=-1066, y=1333278
Sensor at x=2518209, y=2287271: closest beacon is at x=2761373, y=2000000
Sensor at x=3982110, y=3262128: closest beacon is at x=3844688, y=3570059
Sensor at x=3412896, y=3999288: closest beacon is at x=3844688, y=3570059
Sensor at x=2716180, y=2798731: closest beacon is at x=2810772, y=2699609
Sensor at x=3575486, y=1273265: closest beacon is at x=3943893, y=1918172
Sensor at x=7606, y=2926795: closest beacon is at x=103421, y=3007511
Sensor at x=2719370, y=2062251: closest beacon is at x=2761373, y=2000000
Sensor at x=1603268, y=1771299: closest beacon is at x=2761373, y=2000000
Sensor at x=3999678, y=1864727: closest beacon is at x=3943893, y=1918172
Sensor at x=3157947, y=2833781: closest beacon is at x=2914373, y=3062268
Sensor at x=3904662, y=2601010: closest beacon is at x=4001806, y=2787325
Sensor at x=3846359, y=1608423: closest beacon is at x=3943893, y=1918172
Sensor at x=2831842, y=3562642: closest beacon is at x=2914373, y=3062268
Sensor at x=3157592, y=1874755: closest beacon is at x=2761373, y=2000000
Sensor at x=934300, y=2824967: closest beacon is at x=103421, y=3007511
Sensor at x=3986911, y=1907590: closest beacon is at x=3943893, y=1918172
Sensor at x=200888, y=3579976: closest beacon is at x=103421, y=3007511
Sensor at x=967209, y=3837958: closest beacon is at x=103421, y=3007511
Sensor at x=3998480, y=1972726: closest beacon is at x=3943893, y=1918172";

			var list = input.Split('\n');
			var sbs = new List<SB>();

			foreach(var l in list)
			{
				var p = l.Split(' ');

				var sx = int.Parse(p[2].Substring(2, p[2].Length - 3));
				var sy = int.Parse(p[3].Substring(2, p[3].Length - 3));
				var bx = int.Parse(p[8].Substring(2, p[8].Length - 3));
				var by = int.Parse(p[9].Substring(2));

				sbs.Add(new SB(sx, sy, bx, by));
			}

			var minRow = firstPuzzle ? 2000000 : 0;
			var maxRow = firstPuzzle ? 2000000 : 4000000;
			var rowCoverage = new List<(int Start, int End)>();
			var row = minRow;

			for (; row <= maxRow; row++)
			{
				rowCoverage.Clear();
				var ranges = new List<(int Start, int End)>();

				foreach (var sb in sbs)
				{
					if (sb.Top <= row && sb.Bottom >= row)
					{
						var dx = sb.Distance - Math.Abs(row - sb.SY);
						var l = sb.SX - dx;
						var r = sb.SX + dx;

						var i = 0;
						for (; i < ranges.Count; i++)
						{
							if (l < ranges[i].Start)
								break;
						}
						ranges.Insert(i, (l, r));   //sorted by start point
					}
				}

				//merge ranges
				rowCoverage.Add(ranges[0]);
				var rc = 0;
				foreach (var r in ranges)
				{
					var rr = rowCoverage[rc];
					if (r.Start >= rr.Start && r.End <= rr.End)
						continue;   //just skip, fully covered
					if (r.Start <= rr.End && r.End >= rr.Start)
						rowCoverage[rc] = (rr.Start, r.End);
					else
					{
						rowCoverage.Add(r);
						rc++;
					}
				}

				if (rowCoverage.Count > 1)
					break;
			}

			if (firstPuzzle)
			{
				var positions = rowCoverage[0].End - rowCoverage[0].Start;
				Console.WriteLine("Positions: {0}", positions);
			}
			else
			{
				var x = (long)rowCoverage[0].End + 1;
				var freq = x * 4000000 + (long)row;
				Console.WriteLine("Tunning frequency: {0}", freq);
			}
		}
	}
}
