﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2022_3(bool firstPuzzle)
		{
			var input = @"BccTFfTPTsffdDDqsq
lGGLQwFhDgWdqvhW
wbLNjGjlwLFrpSbllrHnHHRmmJVBmZJRRVcBTc
vvGLllBBLtllJnJFMZNjFcNG
bdhrhTgmhRrpLJMMNJgNffnf
qhRmTpzpzVzmTTbmVhWWWpVvDtsLVlwBVHvSsDCvvBDl
sLlhhthVRndBZzwBdV
PfmsPsqsGFqrSQpqBDDwpddDDM
mFjvSFrjPSvLJWvbRssN
gWnWQtMMDQbQvMDjjcwsvqcwsSSqZq
HzPJTtmtZJJsqfBJ
hCFPrNNPVNzFtQhdQMWRhhDd
sNhmsQZdNdsztNpHGCdjcBcwCvGvCv
brSlRfFFppHwfTpf
MbRlRpqMnqRpMhVtsQQQVWsM
HsdttdfHrHrwdhftMHMSDnJFwlQSgQZQZggZSW
LjCTqBCmmmTqgFQQFQjlnnDJ
GCTLBCmTBDBDPTTzqLCBpVcctcHbtbrcMMsdsdtcPtdb
QnJLdNLfLRQjpLlPflfQnlnswRvwsHVWVHWVVDZwWHcrwV
zFbgtqCBhtgBhsDZVDNHwvDv
zgCGCFCFGtMpJjlGTnlnLN
ZFhJZbvZVmFpFnJbnZFbLnbHPrjjQdBdCrMPQMQHrrrrVC
gTzlqmNlTflmtTRHdDPqMjHDMrjdQd
wcRSwglcNSwgWmNzwNlJZLpphbbnFhvLWhnphb
lRRrcbRfQmwwBLSLlvjjSvvFtS
WbghzhHWbsdHhVtGLVSSvLFJtSGC
WWbWzPHDqPfBqRZrmqnZ
frgLHMcHLrSZHPHcHrPNmMJtTCTCbJfhdwVbfJwwTbdd
DpDWRjQvqjDGFWhdVwQCnJgTnTbT
vvqggRsRpcssPrcsrN
MccbcZjmbbNSbjllbRqrhstVTMtqRtssTW
qJdwCLdCzQQzwHLQLTtsTtsTRVTVWWTJst
GdnpFHzwQCdpvpbccPbSbq
CbQhZpTbTgMMgptzrdGtGzlVNlrh
FWvmmRHSmjqnSjqnSjPqjnmNLGVGJlGWLlLrLWlVrGzNGW
FwvvBjmBnqmVvFnvnVPMDspDQTfZMCMTgDQTCB
VmrsHllHVLTdZtRclL
bjwMNQGCjGjjPwpRQnRLTtRQFvcdvt
MbWgjMDpNLMjNpwGpjbPWgfsHsqsHSrrHHJsgBSBzB
HFlwVhfLBZZLTLFHwqWCqWwQpWwSpqWC
PttsNjdtPtcmcDTnDdtjjDMpCCCCmSmqqGqmvMvbCqpC
jndRrTtPdgNPPnLrVhVLHhVJHffV
NJZVqHNNNJNqCdqZZVjtzCDMtzrtrFhhMFsPCS
TbwgwWRdQgfggQgvMPDSPszhSbSDszFs
QpRmWglwTQgGWTvGRvQRdwnjNNqqqBqVlcqVVNncVVHV
gqBFHLFDNCBClHgbLFbllLggVSTWJVWWpjvJpSSWJjBjZvjW
mczdshQcsGftdmcwcfhdzQVrhJZJTZhTVTDjpWpZvvWT
twGdRtcGnRgDqFRC
fsPjCjgRpwjPpsGgQwPfSZcdSZdWVHzSzcdzHRWz
MBLTTBMvbMmvJTLvTDvczNdZNHdHcJNzNqSZVG
rTlDlbBbrsjCCGnG
FsmBPtzHdmmvcvdWpcWCvM
RMJGjGGJJGLDRNJJnfGRRSvplTDccrTwSWClvCrwww
nnRqNGJLVLRRZNNZzBHMHVFVmzBFbhmb
MmTSpBMBCCMsbbrVvwwSLb
RffNRFRThtqHhHHqZNrLVvwQtsGvssdvvsGb
DThHghZfgqhFWRNHhhRWqMCnMzzznlPnBlgzMpnPCz
GhlMNMdpMZHHhhRLLjqSjjqvvmSh
PcTJDBDcBnBbTFcDwnPTQSLqLrqvWjwWLqRSvsRmWm
PDcDJbQTFBCBcncgQPcDgnHgVmppHVMddmZglzVHdMmt
BqFJqJGpBVnJqnjjmwvrNwVPHPrd
ZMbQmhWstZScWjdswwwHNdzHvj
ZZtQCtgZQWSbMMhmMSWQfGfGDBpRJnJqTFFJBgTR
tVcPGGqwgJPqtJtqZZwcZffsfrcNWBnSWWFrfhWBnz
dRMCMQLvNssBWFFR
pvjjQvmDMpHLdPPtgPPTssTHsq
WDBlFBbGdmBrqWjhWcqZCq
RPSRPtncngwgwzhCwghC
cssVsPpVncQMRsVQpRPnRcfJfTTmTdDBLbFlDblTDDDLJf
CZgCCzgzsCDZDzbbBclgvcjcnnjFBqgv
VLGThLWhWdThlWRHVTLTTjNjjdjrnBNBcrNcqmqqcc
VTRWLhpLWHRMPGRGRplVhSfttJpZzwstsbDtwbJzZt
fqhZRLhwZwVSLbCMCJdJCHQGZWdW
ztvpjtpcvgzppPvjvPlDQLnMlHDGWnQDLQWDdJ
PpsPzjPzgLrjzBgLggzrmrVSrTSfSqFVFmVhffFm
qMFDRLNRRJJphbhSgStpptbj
rZrPZZNCCCGrlfsCzBbgstBwtvbtvbvBth
CfVrdndzZGrfzrzdrGddPnNCFMRDJWRDDnRWDFqFDJJFTJJT
dMDbndBMQWnnbDstnMbMQQwspFgsFFjRrqCCFrrprqrgjc
SlLzZmHZSTVGSPVmSPlSmrpFvFjjpgrpgFcvrcFcWG
PHZZTmNVHSWJlHPNLSzPLLHwDBJtbDbnnnMffDbwtnMndQ
tHBzNCztLBRBtrjvSjfnjvvzfpjj
gQwWqlnqWqJgJnDplfflddjdsfdpFf
gcwwmnDccTPWWgJbZNNbZHZCRRTrLB
ZhjgtrJNfDNpqbhqQmqpmb
cFLcwGGHwcGdwFCQjnnnVcmqmjCp
HvvlzjWHjFWTggWStDrDZT
GPjjQtPQbjwWqrmnsjmnqn
SNGdfLLGdlldZSSRWznFsNqFRnqsWs
DdMpMfZMhfZMpDfbDQQbGQVgccgcVt
hHGGGTlddWGgpRdcvwDCDwzgLJzCwzwgwL
MFSbZSnnFJWwQBNMzw
tSjrSFSfqjqqttPnssqjssbhGmWGhcmvldcmhHRhGRhTRf
qdBjBTNndbnqnLmtZmZvvtLvHd
hDJCpfnGhJfDPzGzzCnGPmpZZVLZvHttZHQLHgZLHt
JDrfCDJhGMhFhPzMrCCnrGSwswswFsblTqRlRwjcjRsqBT
tJPRSZCSJJCnmvvvQMrpqLVwqLqMcCCM
hhGGGfsdfTGlfggjMLVVFsMVwMMbqbLF
hdTGGhGhdhfhhwlgNfdhZnZtHHRNtZHnnZmHHzmn
HlgRZglZDWZgfVbdznHddTvV
MhShhQMSFShFPPQplMPmwppwbzdtVbFtfvfbzVbvbTntzbVT
lMwpmJLLLNGwBrcZCjrGCGrD
qjjWRLjNjtGRRWTCghNwsgwcbwmC
HPMBpVGMPMvvVBPswmhTbCwgmsVThT
vDSMSvflBlHpvMMfSSpMFWZdtZWdGWGddqfFtRqQ
ZmnGQfnZgdmRGQGvgnnmHCbbchhpMLrpcbLpdLpzbz
NBFPBWFsSVNJlFqLDLwcwrPLzzCwhp
qlWJqqWFJJjNqJWsFVsVqqRCggtmvvjQZgQnZQmvmHRt
RdCsJbdsVJtRvdzBzrBcjzMTqT
wNSNnnHhnwhHfBHqHjBDJMJr
GnSPLwlFwwLWSwpNWLSNpVZtvPsVJsmRCmtgsvsRsv
GPMwMMPCBPwBGsCGGWqBsslQhVQFccphvhWpmcFhVLmV
rbNnHLLHfHfZFfQQQZml
DzSDNtrztbgCMMBLLCsBts
rTtrVbrrhbbGGhbbbbRqccdBcdBcvRvBSRRV
fDqQDqLFQgQgZFMlFNRgvCNvRRvHvRBHHR
mfPqmFsqqsbJJtGtbt
vNHvgsSGSFDHvspvtSGwJwJNJrllhwhcnwJlwm
dqWdWfQdqQVWwnfMnlcfwzMs
VqQQqBRTqsBQWLppZLpFSHZbvvDt
gdcldHQlQndnHMzCjvCFrzjSFtbLtv
NJGmZZJZGTDsWWJNmDVmsCrSvfPrvTtSPbFFCFFvCT
BDDsmJZVBJwBRsGGDmZNBGqMdnghhcwgbngpqqMgnpgc
VhhvVwmvmwTPCwPwmDRgDCsgWSnfRMSWSM
ptHZZtlZzqbZttHbzrFqnDSMShMgSRRMngDWgrGh
hBttqBhBzlFhtHvTJQJTcvBQJPPv
FjfzfGjsjBfQfMLBNg
VlrppVwrpVSdScwTjVnCCQLQNCMBhWgV
jvtSrSjjtSZpqtHPDPJqRzGR
ZSmbSDswfCDDHBFFvWHJ
jcjcVjltntQMltnVrdNnNjdQgFzWHqzBWGWQvzHwgqqJGz
nccLllwwhLCCSLTmmRTP
HBSnnJSfHvBfNWMNrvnMrZlblFbsbHwsPFVHTsHFls
VLmDLhRgRbZFRwjZ
zQDmCLLDDLGttGGgtLvvVSfqJBSVftSnrJnN
CgGhbgVMNgVVbjrrtcfTDTfvTscrTvSSss
qqFzFBzsPZHmddmsmvWSnHJnncDfWnDJvv
ZwdPqdqpdPlPwdRlsdBqBMjNQjGVgbQgQhGVCCtgbR
CGFTTMLGPgmPfGfCwTPSSFNQDLNQnscQNccWdQLqvqds
pbJlzhZRHtjrbrbjHHrplRWVNsNvddZNWDQVVcnQVscs
pzJtlJlBhJJHJjHhfPfTTFMDmMPBMSgm
gzCBPDDzgvLvgPLgNThPlVZccJTmrZNV
sndSnpsdMSnRMRpjShhNJZJhJjrVcTljTZ
SRRsdnGwRSpptnfMSSpdQtfdWqvbwFgbDJJzCzqqWqWbLWWz
BDnsPDlmmwcnCLLLwPtFTtTtFRqjRrrSVFqn
dfhhzMGzWJhpMWhHWggTHJVFVSSqbqqjdRRtvqqvFrtF
JMGfHffhQhQTNcTTBLCQmm
bfZBvvRRRzFfFFLRvFzZCcQlScchLlGNhSQGGVQh
mwJqTbsHmjbTNcNhQGGJGcVS
mPTgtsnPjwHHmmmbbRDgfpdgBpzvZD
lSnRStHtTZdjrHjnqJglbqgchhDCPCPc
swFBzvBNLpBBsvszvDJhCzgDDCgbbJQzDq
VswvBFmvpSrShmRRMn
vWBBSrWnZfCWVchwhbcjVN
RdHQQpRPJZLTtJgNcNGgbhGh
zpzDzTdqQRqRzzlRDsBBnBfMmCCSZDBS
ZtGSZVpPDtVbQjbwBDzbbL
WcFvTFnTMnnMcnhmQhmhBbBQzCQjVb
RRWsgvgnfqgpGVNg
WJTrJJCzLqCqBTWLsCCqzmPPQrjwHQQGpwGHHmRPVw
bMvnDFnFSbSlGgnpmGQVpRjV
DZSdFZdQZZclsscWqWLhcBhs
PzLlRRNjjRQzvPNQsvddlZfchhWWZJHSlhChDhHhcc
MMfngVwtpVMqVrwrMBgmtGqCHDWqJSDJWhHHSSSJJHHD
tBgnMTMMrgVmrBwMmGfnNjjsdLbbbTQvjNTNsQLz
ZZBZRmPmgpgZGLWLQWslSWmLQL
DnHJJjzqrJffrDnHzJjnMbQWSltttSbStvFQSstSvCLF
MzJDnfzHwHlljJJnqrMjfPGdBRpRBGBRhwVdGGRTBG
CnZCpMFNnFvvNdpHVrWghgtFVFlLRWhh
cBsSBsGcjGcJZDrggtgtVDVrgWmh
wTBJbcbzffJbZccjSbSjBfccNCNMPqqPPdvnHvTvnMTNdnvQ
vMgPmvQmWDMpGpjBbMMH
CcVJNcdNgdhtCVpjBBRppfRTGbph
sFsgNlcdFlJFFwFstNJcvzDWZqqWvqLqzLzmzDwQ
LLVLVsPPVVPCLLrjCNNNgmRdJNdCdfMJpB
DTZZHTWbwwpWbSWDBmJMSFFNmgRRBggf
ZvzDWqDnDwnZTpzZTzWvphPtqhQsltVtPhPhsQrPch
zDgWmDgrpCLmwgWTrjlJBQRJjbFGrcbQ
hSMvqvHtqsdVHlJDcclMBjRMQJ
sqvHsSstSdqhVVvZdqVHZDgwWpZZLLmfmmwfNfWCgmLg
SQWcTnWVWbZWWBcVPnZVbnrNrMFMdqFNqdMqqFhrDQvq
plGLlLGpJLhCGrRqGDDDrdzNGr
wgflHCCCJmpLjCLHtjjgLCtBZswnsTBPVPVsBcPTbZBBSh
vvlMQvvdjdGtVCTJlVJVfJ
FLrFqwwZgNrFWqZwgqrZBLWcBTtppztVbfRJztJbztfztT
WNmmFJwwrFFnNmDgmjdGQMdHMsPvPjsHDG
hGmZHdSRdMmhMZSHlvbTvRbRlVtCTlCR
znnfzgPPDpPfDcgnZTJvJNCZbJVCcNJV
nrLLfQznprrppgprWrnPzQzLSjGsZmHmhBdsqWhdhBMWhdqd
zhtNFSFwRFLCsNrNNBdl
BmQBPjDpBTDgHllgHc
npBjjpQpjGbMnmPpjPQWpwZhtbzJfhwvwtSwhhFFbh
PZcZbcPlbSprcQbbdCwWRSttgtgvWfjC
TGVLVHHmTVHGDTDnGDhgWjwvCjwwwRLLgBjWBR
CHTGsnHVVcJPPcNsNJ
tTqGSSGPGfVfTpqGTbbcVWJLdjtvdzjJthCjlhdlzQ
wwFBZMmZBmgnjzlCWBBjBLjv
rNMsDZnMMWSfDcDWPR
vLzbsczhLmmnlNvrNQHfWd
SMSFqMwjFFDVSZwVTMDjSQlQfNlRrQRWdQfRrWrqHN
CPTwGZDTFCPSjFTSPSFbPgpLscPczmcBLbfgpb
QnQnpFjsbFcSSvCMNvqVSrqq
WfzfTfzzPgHTfwfWtgRLMJDvjMmMVtvDJJCVtqmC
wfWRgPzdgRTWBBWHPBHHBRLTlcZdhjnbZQcGZGpsnphjshbG
pHzPTsBHzqqtQCZZshlWjf
DFFbnvJMDMljjtQjfCbQ
dgJFDGwgmGlMSggGdgdDDlvLNTzpBzLzzwpTBLzqPBczLT
jccNVNdwnclRwlbwlVjdcpJSpGpSllBHgGHZpJpppf
mDThTmsnDsSBpZfmmgBf
MrFrhCshqvWvnWzTWQtzVbRRQwQRNQjdwQ
tBnLJfnQtzRCffmNjSRjZjNZSRrP
dVdMVMvMghHzPhzZhHND
dGWGgplWGVMdMMzCTsbLtLTCLpnT
ftNfNDdSBdrMTdrjMM
cHgHGHzGgJhrPLqSrrJTqp
mVFnQnhQGHFznFhBBbSDRBlSVBBRfS
VqqPBPcPbQHgfrrpcSDR
tpMnsztnGnthhzTtGTGTzzWgJNDlMfSRlRDgRNRrDRHfrg
zWtTFWzTwphChnCzFhzWZGGvBvLqmVmbVQqjqjmBPqBLCP
ZZgZnhrmwmnmgmvrghPmgTGcTSGSMSldgcCQCSqW
LzFLDBfHzHCCqCFGcSlS
HBjRJDLpHpJsJVJqsnhP
PqrqmvmrwzznnPDpjVpDLfDtPGLt
sdRhRWFhShhFccZZsSsNbsNcjCtLpMVWCjptGCfMftBDLMtG
bVZcsdlhdNSbZRSshRcbbqlmnqHmmwzrJlzzgQlmvz
pnrcNGqmrGqnchGhqdWdTlldtQtlMsTq
DvSLgzLSMfbgggCLCwbSSLLtfsWTQTsWllssdltRQQtttQ
bCCzzPPgDPjPvwSzDbwpVNnpnjjrhpnVZGMhcp
jWbGtDdqCqZjdHwcwZMBVQmcvZ
PnTflPRRrlgLTTRlTzFPPQQBcNvHBncQpHMwHNBMwm
fFFRrFLJgRcJglgRzTzrLqWCjtqGGDsjCjdbGdqdhJ
GwbvGqMsDMbpMGzzgRzgpBLjhcch
WFTFNZTZSCcBggBFcrss
CJWWlsWlCtqGJmMGwJ
CGCVhprTrthCZTCNtVGtZDZNdlPPdPwmmvrcbmPmdQRvQWmw
LfzLzssfgHjLFjFLfjMfHsLHmPwdcQWQQlscwlPdQclbPvlw
LBFHjgMzqqjfJqLMzffHzqgHhhJDZSGVDVChCDDpDNNpNtDW
TbzVlmNTVVtnTSWNwDDrpGcwdp
fQQMFbhCfLgfQCsdDcHpsWpdSDsGrr
ghvPLQMfZhjjvPLhbQFQBZqJlTnnnVzRmtRzlmBl
JCLLLwVDwCQsNwwJHmfrMZpMfMMrfPQSMZpS
WlFlzFRnznthqWRGbMpVMbbMPtMjMj
vTnvzqllhdhqTwVBLcJHmmmC
tczhtcJJJbtclWrtJBWJBtJtpqPRSPfpBRgqRfPmpRqddSmM
HCvnsQLNCQwLnDsNHLwQfPSSpPSMfnPddRMmmGpp
wDQwjNwQNHjTHNFDCNmCFNWtWzhJbWbVrhtWccVFlrlV
sPRpCndBCGpCGHttSdvTbWvgdjST
wcmDwqcwmGDTNvjWtrbSrc
lwlVLVGqZGlLzVHHBBBCHBHRPCCz
wdmhffzzphrjqtzRbrrq
CRgGTGTFssZsllHNBlHsFJRjcrtjtPDPcbCtDrLcrjctrc
NHMRHTlFgGNwpQvMpwVvww
MPLJNPqmFWmDFjGS
nbsZtwbZlbZlGlFDDMpVlF
hvsbbbZtvfhhRZbZsfzMbMZbqcrqdrNrNqLgqhrLhJgddLNL
SnMLpRDGlZSZNlnMZpCwjLwzFrHBWCFWBBBr
TttvvtbtVcsJtRsvtQdzWJrBjCCHBWzrFFhCjC
VmvsQdgPbdgVTvgPMSDfPPZfDRRNDNMl
LjngLCNhDNFNhFDhcMqrqqZMcSZnHTMc
PPJwtGlfszGwWtzwQJBPGslJSqRTHZvgcRRZrMSTMMTtRTTc
llPPJWzQPmWmVNgDbLFCLb
NPFlLNBLprpdmmdPBmJnLrdjMVDjMSdqgggQTVDqWMdqVs
vZTbTZRwvvGRTRjWDqSqqQgDGsGq
vzZcfRRZbwbRHRtwZCChBmFNtJBPLBlBJJLpmFTP
pmvZmmTjQFfnvPPHHv
SczhzfbsLNhfccNFsWFRPrDnPDnVnW
BtBbSdtzLBwSLwBmTZpQMpZmZmfZ
PqPQZqtQQLDqrnqdjqdVwVbz
MGRGWMgJHGlRRHfSwfzCfCVVQzbwjrjn
JsmHmSJJmSMWMlTWQBFhLTvhDFZhBQDZ
sbgbbFGTTFNMbMNFWrjsrvWzHWPzPPpf
CVmhVqSqCZmJQhPpHzvZvtzWHjHP
CnmdCQCdnFTnNgRpwT
blZjhbZWVttjWjWLCLVVZCZQjMDQHsBsBQfMDQwjHDwBHH
NdcJdFcJqgpJpNnDQLMFsBnLSnnS
LJrJdJrzvdrrpcNdNcrVlZWbZzZVRhRtVlPttC
mhRtNNtrtBQQrtrBBmQlZwHHqHZSVHHGshSVDwhS
gpdPMTcsLscMccTpbLdHSfGfqwHZDHHqZqHZ
zbzvMpPLppLzLMjTBQRmssjBWRQjlmrN
VzzvggdvFdmffwmGpd
HNbnJTRTmCwwrRpR
LlTnWhLlhLJmLmtZtPcPcVFFPSZgZt
jHcZjHlHzLHHnSNSfL
pQWRrwPwrRWBWBPWBRrpdPmzhShsSFFNShLhnnvPtvSNNs
dzbmwVwbbBmGcqDbgllJCC
ndnvvzJDHvzHHHjnHjCCSDLgbSFwNFVbFVTL
mcQmQtpWTQGlmpTtMtqtpqTFsLcVSSscNCwLNLbwbbLNwV
pRZTQlhmtGWqqWnPHdnhjHrBjPPd
TtLpNHspTcLNNsLpthhsfmtjRSRlWWbzSwSRGwbWlWSSvlmS
qBJVnZZdJVZrZndbPbWwRzSMVGbbVS
QCdnBFBndBQDnrqrnqqNhpNNLpHthsThjGCTLL
phCgcdrFbPLpgrbFHqQqzzlbGWGqQbHW
SFTvTnVVMRnNTNfSHjHQMDlHwDWlQwDz
TZmTvsFNmvTtsggpdJLBBsCs
PBBWQjvsPsHVsNMcSzNDjcGggS
ZtrTfTrrrrdCqpdtLNnMLLqNcgMzgHLq
CZFmdTrJtbZrBvWHVVvHbPQW
djcrrBljMrTdCTcdCClClMlqRvtNqqSRwFbNbwvNBNpSzq
QhPmGJnPVGVHHNzSqpzFwztF
nhgPFmsnLPGLhPDJhGTcDjMfrMMjMZWfjfWj";

			var rucksacks = input.Split("\n");
			var result = 0;

			if (firstPuzzle)
			{
				foreach (var r in rucksacks)
				{
					char shared = char.MaxValue;
					var items1 = new HashSet<char>();
					var items2 = new HashSet<char>();
					var half = r.Length / 2;
					for (int i = 0; i < half; i++)
					{
						var i1 = r[i];
						var i2 = r[half + i];

						if (i1 == i2 || items2.Contains(i1))
						{
							shared = i1;
							break;
						}
						else if (items1.Contains(i2))
						{
							shared = i2;
							break;
						}
						else
						{
							items1.Add(i1);
							items2.Add(i2);
						}
					}

					if (shared == char.MaxValue)
						throw new Exception(String.Format("Invalid input {0}", r));

					result += char.IsLower(shared) ? (shared - 'a' + 1) : (shared - 'A' + 27);
				}
			}
			else
			{
				var groups = rucksacks.Length / 3;
				for(int g = 0; g < groups; g++)
				{
					var r1 = rucksacks[g * 3];
					var r2 = rucksacks[g * 3 + 1];
					var r3 = rucksacks[g * 3 + 2];

					var i1 = new HashSet<char>();
					foreach (var ch in r1)
						i1.Add(ch);

					var common = new HashSet<char>();
					foreach(var ch in r2)
					{
						if (i1.Contains(ch))
							common.Add(ch);
					}

					char shared = char.MaxValue;
					foreach(var ch in r3)
					{
						if (common.Contains(ch))
						{
							shared = ch;
							break;
						}
					}

					if (shared == char.MaxValue)
						throw new Exception(String.Format("Invalid input {0}", r1 + "\n" + r2 + "\n" + r3));

					result += char.IsLower(shared) ? (shared - 'a' + 1) : (shared - 'A' + 27);

				}
			}

			Console.WriteLine("Sum: {0}", result);
		}
	}
}
