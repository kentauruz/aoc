﻿using System;
using System.Collections.Generic;

namespace AoC
{
	partial class Program
	{
		private static Dictionary<int, Action<bool>[]> Puzzles = new Dictionary<int, Action<bool>[]>
		{
			{ 2015, new Action<bool>[]
				{
					Day_2015_1,
					Day_2015_2,
					Day_2015_3,
					Day_2015_4,
					Day_2015_5,
					Day_2015_6,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null
				}
			},
			{ 2020, new Action<bool>[]
				{
					Day_2020_1,
					Day_2020_2,
					Day_2020_3,
					Day_2020_4,
					Day_2020_5,
					Day_2020_6,
					Day_2020_7,
					Day_2020_8,
					Day_2020_9,
					Day_2020_10,
					Day_2020_11,
					Day_2020_12,
					Day_2020_13,
					Day_2020_14,
					Day_2020_15,
					Day_2020_16,
					Day_2020_17
				}
			},
			{ 2021, new Action<bool>[]
				{
					Day_2021_1,
					Day_2021_2,
					Day_2021_3,
					Day_2021_4,
					Day_2021_5,
					Day_2021_6,
					Day_2021_7,
					Day_2021_8,
					Day_2021_9,
					Day_2021_10,
					Day_2021_11,
					Day_2021_12,
					Day_2021_13,
					Day_2021_14,
					Day_2021_15,
					Day_2021_16,
					Day_2021_17,
					Day_2021_18,
					Day_2021_19,
					Day_2021_20,
					Day_2021_21,
					Day_2021_22,
				}
			},
			{ 2022, new Action<bool>[]
				{
					Day_2022_1,
					Day_2022_2,
					Day_2022_3,
					Day_2022_4,
					Day_2022_5,
					Day_2022_6,
					Day_2022_7,
					Day_2022_8,
					Day_2022_9,
					Day_2022_10,
					Day_2022_11,	//TODO
					Day_2022_12,
					Day_2022_13,
					Day_2022_14,
					Day_2022_15,
					Day_2022_16,	//TODO
					Day_2022_17,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					Day_2022_25	//TODO - missing stars
				}
			},
			{ 2023, new Action<bool>[]
				{
					Day_2023_1,
					Day_2023_2,
					Day_2023_3,
					Day_2023_4,
					Day_2023_5,
					Day_2023_6,
					Day_2023_7,
					Day_2023_8,
					Day_2023_9,
					Day_2023_10,
					Day_2023_11,
					Day_2023_12,
					null,
					null,
					null,
					null,
					Day_2023_17,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null
				}
			},
			{ 2024, new Action<bool>[]
				{
					Day_2024_1,
					Day_2024_2,
					Day_2024_3,
					Day_2024_4,
					Day_2024_5,
					Day_2024_6,
					Day_2024_7,
					Day_2024_8,
					Day_2024_9,
					Day_2024_10,
					Day_2024_11,
					Day_2024_12,
					Day_2024_13,
					Day_2024_14,
					Day_2024_15,
					Day_2024_16,
					Day_2024_17,
					Day_2024_18,
					Day_2024_19,
					Day_2024_20,
					Day_2024_21,
					Day_2024_22,
					Day_2024_23,
					Day_2024_24,
					Day_2024_25
				}
			}
		};

		static void Main(string[] args)
		{
			var year = 2024;
			var day = 25;
			var first = false;

			Puzzles[year][day - 1](first);
		}
	}
}
