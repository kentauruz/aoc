using System;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace AoC
{
	partial class Program
	{
		static void Day_2015_4(bool firstPuzzle)
		{
			var input = @"bgvyzdsv";

			var input2 = @"abcdef";
			var zeores = firstPuzzle ? 5 : 6;
			var prefix = new string('0', zeores);
			string output;
			var i = 0L;
			
			using (var hash = MD5.Create())
			{
				while (true)
				{
					output = string.Concat(hash.ComputeHash(Encoding.UTF8.GetBytes(string.Concat(input, (i++).ToString())))
						.Select(x => x.ToString("x2")));

					if (output.StartsWith(prefix))
						break;
				}
			}

			Console.WriteLine($"{i - 1} -> {output}");
		}
	}
}
