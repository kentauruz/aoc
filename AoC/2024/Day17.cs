using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.Marshalling;

namespace AoC
{
	partial class Program
	{
		static void Day_2024_17(bool firstPuzzle)
		{
			var input = Day_2024_17_input;
			var lines = input.Split('\n');
			var A = 0L;
			var B = 0L;
			var C = 0L;
			List<long> program = null;

			foreach(var line in lines)
			{
				if (line.StartsWith("Register A: "))
					A = long.Parse(line[12..]);
				else if (line.StartsWith("Register B: "))
					B = long.Parse(line[12..]);
				else if (line.StartsWith("Register C: "))
					C = long.Parse(line[12..]);
				else if (!string.IsNullOrEmpty(line))
					program = line[9..].Split(',').Select(long.Parse).ToList();
			}

			List<long> Compute(List<long> program)
			{
				var ip = 0;
				var output = new List<long>();

				while(ip >= 0 && ip < program.Count)
				{
					var instruction = program[ip];
					var literal = program[ip + 1];
					
					long Combo(long literal)
					{
						return literal switch
						{
							>= 0 and <= 3 => literal,
							4 => A,
							5 => B,
							6 => C,
							_ => throw new Exception($"Invalid combo literal {literal}")
						};
					}

					if (instruction == 0)	//adv
					{
						A /= (long)Math.Pow(2, Combo(literal));
					}
					else if (instruction == 1)	//bxl
					{
						B ^= literal;
					}
					else if (instruction == 2)	//bst
					{
						B = Combo(literal) % 8;
					}
					else if (instruction == 3)	//jnz
					{
						if (A != 0)
						{
							ip = (int)literal;
							continue;
						}
					}
					else if (instruction == 4)	//bxc
					{
						B ^= C;
					}
					else if (instruction == 5)
					{
						output.Add(Combo(literal) % 8);
					}
					else if (instruction == 6)
					{
						B = A / (long)Math.Pow(2, Combo(literal));
					}
					else if (instruction == 7)
					{
						C = A / (long)Math.Pow(2, Combo(literal));
					}

					ip += 2;
				}

				return output;
			}

			if (firstPuzzle)
			{
				var output = Compute(program);
				Console.WriteLine(string.Join(',', output));
			}
			else
			{
				var res = new Dictionary<int, List<long>>() { { 0, new List<long> { 0 } } };
				for(var i = 0; i < program.Count; i++)
				{
					for(var j = 0; j < res[i].Count; j++)
					{
						res[i][j] <<= 3;
						var a = 0L;
						for(; a < 8; a++)
						{
							A =  res[i][j] | a;
							B = 0;
							C = 0;

							var output = Compute(program);
							if (output.SequenceEqual(program[^(i + 1)..]))
							{
								if (!res.TryGetValue(i + 1, out var results))
									res.Add(i + 1, results = new List<long>());
								results.Add(res[i][j] | a);
							}
						}
					}

					// foreach(var r in res)
					// {
					// 	foreach(var c in r.Value)
					// 	{
					// 		A = c;
					// 		B = 0;
					// 		C = 0;
					// 		var outputCheck = Compute(program);
					// 		Console.WriteLine($"{c} {string.Join(',', outputCheck)}");
					// 	}
					// }
				}

				foreach(var c in res[res.Count - 1])
				{
					A = c;
					B = 0;
					C = 0;
					var outputCheck = Compute(program);
					Console.WriteLine($"{c} {string.Join(',', outputCheck)}");
				}
			}
		}
		
		static string Day_2024_17_input = @"Register A: 21539243
Register B: 0
Register C: 0

Program: 2,4,1,3,7,5,1,5,0,3,4,1,5,5,3,0";

// 2,4 bst B = A % 8
// 1,3 bxl B = B xor 3 (011) 
// 7,5 cdv C = A / (2 ^ B)
// 1,5 bxl B = B xor 5 (0101)
// 0,3 adv A = A / (2 ^ 3) (8)
// 4,1 bxc B = B xor C
// 5,5 out B % 8
// 3,0 jnz A != 0 goto 0
	}
}