using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace AoC
{
	partial class Program
	{
		static void Day_2024_11(bool firstPuzzle)
		{
			int DigitsLog10(long n) => 1 + (int)Math.Log10(n);
			
			var input = Day_2024_11_input;
			var stones = input.Split(' ').Select(long.Parse).ToList();
			var cache = new Dictionary<(long, int), long>();
			var iterations = firstPuzzle ? 25 : 75;
			var count = 0L;

			long GetCount(long number, int iter)
			{
				if (iter == iterations) return 1;	//we are at the bottom
				if (cache.TryGetValue((number, iter), out var result))
					return result;

				var count = 0L;
				if (number == 0)
					count = GetCount(1, iter + 1);
				else
				{
					var digits = DigitsLog10(number);
					if (digits % 2 == 0)
					{
						var half = Pow10(digits / 2);
						count = GetCount(number / half, iter + 1) + GetCount(number % half, iter + 1);
					}
					else
						count = GetCount(number * 2024, iter + 1);
				}

				cache.Add((number, iter), count);
				return count;
			}
			
			for (var i = 0; i < stones.Count; i++)
			{
				count += GetCount(stones[i], 0);
			}

			Console.WriteLine($"Stones: {count}");
		}
		
		static string Day_2024_11_input = @"7725 185 2 132869 0 1840437 62 26310";
	}
}
